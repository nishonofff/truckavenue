package com.example.truckavenue.di.module;

import android.content.Context;

import com.example.truckavenue.BuildConfig;
import com.example.truckavenue.data.network.ApiService;
import com.example.truckavenue.data.network.ApiServiceImpl;
import com.example.truckavenue.data.network.TweakAPI;
import com.example.truckavenue.data.prefs.PrefsService;
import com.example.truckavenue.utils.AppConstants;
import com.readystatesoftware.chuck.ChuckInterceptor;
import com.squareup.moshi.Moshi;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.moshi.MoshiConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */

@Module
public class NetworkModule implements AppConstants {

    @Provides
    @Singleton
    static ApiService provideNetworkService(Context context, TweakAPI tweakAPI, PrefsService prefsService) {
        return new ApiServiceImpl(context, tweakAPI, prefsService);
    }

    @Provides
    static TweakAPI provideAPI(Moshi moshi, OkHttpClient okHttpClient) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_API_URL)
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .addConverterFactory(ScalarsConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build();
        return retrofit.create(TweakAPI.class);
    }

    @Provides
    static OkHttpClient provideOkHttpClient(Context context) {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);

        OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder();
        if (BuildConfig.DEBUG) okHttpClientBuilder.addInterceptor(new ChuckInterceptor(context));
        okHttpClientBuilder.addInterceptor(interceptor)
                .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS);

        return okHttpClientBuilder.build();
    }


    @Provides
    static Moshi provideMoshi() {
        return new Moshi.Builder()
                .build();
    }
}


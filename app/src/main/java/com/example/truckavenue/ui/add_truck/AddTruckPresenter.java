package com.example.truckavenue.ui.add_truck;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.truckavenue.App;
import com.example.truckavenue.R;
import com.example.truckavenue.data.network.ApiService;
import com.example.truckavenue.data.network.entity.truck.TruckModelResponse;
import com.example.truckavenue.data.network.entity.truck.TruckResponse;
import com.example.truckavenue.utils.AppUtils;

import java.io.File;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

@InjectViewState
public class AddTruckPresenter extends MvpPresenter<AddTruckView> {
    private static final String TAG = AddTruckPresenter.class.getName();

    @Inject
    ApiService mApiService;

    private Context mContext;

    AddTruckPresenter() {
        App.getAppComponent().inject(this);
    }

    @SuppressLint("CheckResult")
    void addTruck(Context context, String truckNumber, String truckModel, Uri imageUri, Boolean isMain) {
        mContext = context;

        if (mApiService.noConnection()) {
            getViewState().showNoInternetConnection();
            return;
        }
        getViewState().showProgress();

        MultipartBody.Part imagePart = null;
        Uri imageURI = null;

        RequestBody truckNumberPart = RequestBody.create(MediaType.parse("text/plain"), truckNumber);
        RequestBody truckModelPart = RequestBody.create(MediaType.parse("text/plain"), truckModel);

        if (imageUri == null) {
            imageURI = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE +
                    "://" + mContext.getResources().getResourcePackageName(R.drawable.photo_add)
                    + '/' + mContext.getResources().getResourceTypeName(R.drawable.photo_add) + '/' + mContext.getResources().getResourceEntryName(R.drawable.photo_add));
            imageURI=Uri.parse("android.resource://com.example.truckavenue/drawable/photo_add.png");
        }

        String partImage;
        File image = null;
        if (imageUri == null) {
            image = new File(imageURI.getPath());

        } else {
            partImage = AppUtils.getPath(mContext, imageUri);
            if (partImage != null) {
                image = new File(partImage);
            }
        }


        RequestBody body = RequestBody.create(MediaType.parse("image/*"), image);
        imagePart = MultipartBody.Part.createFormData("photo", image.getName(), body);


        mApiService.addTruck(truckNumberPart, truckModelPart, imagePart, isMain).subscribe((TruckResponse res) -> {
            getViewState().hideProgress();
            getViewState().addSuccess();
        }, (Throwable throwable) -> {
            Log.e(TAG, "Get categories API method is finished");
            Log.e(TAG, throwable.getMessage());
            getViewState().addError();
        });
    }

    @SuppressLint("CheckResult")
    void updateTruck(Context context, int truckId, String truckNumber, String truckModel, Uri imageUri, Boolean isMain) {
        mContext = context;

        if (mApiService.noConnection()) {
            getViewState().showNoInternetConnection();
            return;
        }
        getViewState().showProgress();

        MultipartBody.Part imagePart = null;

        RequestBody truckNumberPart = RequestBody.create(MediaType.parse("text/plain"), truckNumber);
        RequestBody truckModelPart = RequestBody.create(MediaType.parse("text/plain"), truckModel);

        if (imageUri == null) {
            mApiService.updateTruck(truckId, truckNumberPart, truckModelPart, isMain).subscribe((TruckResponse res) -> {
                getViewState().hideProgress();
                getViewState().addSuccess();
            }, (Throwable throwable) -> {
                Log.e(TAG, throwable.getMessage());
                getViewState().addError();
            });
            return;
        }

        String partImage = AppUtils.getPath(mContext, imageUri);

        if (partImage != null) {
            File image = new File(partImage);
            RequestBody body = RequestBody.create(MediaType.parse("image/*"), image);
            imagePart = MultipartBody.Part.createFormData("photo", image.getName(), body);
        }

        mApiService.updateTruck(truckId, truckNumberPart, truckModelPart, imagePart, isMain).subscribe((TruckResponse res) -> {
            getViewState().hideProgress();
            getViewState().addSuccess();
        }, (Throwable throwable) -> {
            Log.e(TAG, throwable.getMessage());
            getViewState().addError();
        });
    }

    @SuppressLint("CheckResult")
    void getTruckModelList() {
        if (mApiService.noConnection()) {
            getViewState().showNoInternetConnection();
            return;
        }

        getViewState().showProgress();

        mApiService.getTruckModelList().subscribe((TruckModelResponse res) -> {
            getViewState().hideProgress();
            String[] modelList = new String[res.result.data.size()];
            int i = 0;
            for (TruckModelResponse.TruckModel truck : res.result.data) {
                modelList[i++] = truck.model;
            }
            getViewState().setModelList(modelList);
        }, (Throwable throwable) -> getViewState().addError());
    }
}

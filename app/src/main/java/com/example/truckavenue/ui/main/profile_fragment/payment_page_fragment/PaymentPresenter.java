package com.example.truckavenue.ui.main.profile_fragment.payment_page_fragment;

import android.annotation.SuppressLint;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.truckavenue.App;
import com.example.truckavenue.data.network.ApiService;
import com.example.truckavenue.data.network.entity.CardListResponse;

import javax.inject.Inject;

@InjectViewState
public class PaymentPresenter extends MvpPresenter<PaymentView> {
    private static final String TAG = PaymentPresenter.class.getName();

    @Inject
    ApiService mApiService;

    PaymentPresenter() {
        App.getAppComponent().inject(this);
    }

    @SuppressLint("CheckResult")
    void getPaymentsList() {
        if (mApiService.noConnection()) {
            getViewState().showNoInternetConnection();
            return;
        }
        getViewState().showProgress();

        mApiService.getCardList().subscribe((CardListResponse response) -> {
                    getViewState().hideProgress();
                    getViewState().setPaymentList(response.result.cardList);
                },
                (Throwable throwable) -> {
                    getViewState().hideProgress();
                    getViewState().errorInLoading();
                });
    }
}

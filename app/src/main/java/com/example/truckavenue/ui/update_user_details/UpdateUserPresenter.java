package com.example.truckavenue.ui.update_user_details;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.truckavenue.App;
import com.example.truckavenue.data.network.ApiService;
import com.example.truckavenue.data.network.entity.auth.response.UserResponse;
import com.example.truckavenue.data.prefs.PrefsService;
import com.example.truckavenue.utils.AppUtils;

import java.io.File;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

@InjectViewState
public class UpdateUserPresenter extends MvpPresenter<UpdateUserView> {
    private static final String TAG = UpdateUserPresenter.class.getName();

    @Inject
    ApiService mApiService;

    @Inject
    PrefsService mPrefsService;

    private Context mContext;

    UpdateUserPresenter() {
        App.getAppComponent().inject(this);
    }

    @SuppressLint("CheckResult")
    void getUserInfo() {
        if (mApiService.noConnection()) {
            getViewState().showNoInternetConnection();
            return;
        }
        getViewState().showProgress();

        mApiService.getUserInfo().subscribe((UserResponse response) -> {
                    getViewState().hideProgress();
                    getViewState().setUserInfo(response.result);
                },
                (Throwable throwable) -> {
                    getViewState().hideProgress();
                    getViewState().updateUserError();
                });
    }

    @SuppressLint("CheckResult")
    void updateUserInfo(Context context, String userName, String phoneNumber, String email, Uri imageUri) {
        if (mApiService.noConnection()) {
            getViewState().showNoInternetConnection();
            return;
        }
        mContext = context;

        getViewState().showProgress();

        MultipartBody.Part imagePart = null;

        RequestBody userNamePart = RequestBody.create(MediaType.parse("text/plain"), userName);
        RequestBody phoneNumberPart = RequestBody.create(MediaType.parse("text/plain"), phoneNumber);
        RequestBody emailPart = RequestBody.create(MediaType.parse("text/plain"), email);

        if (imageUri == null) {
            mApiService.updateUserInfo(userNamePart, phoneNumberPart, emailPart).subscribe((UserResponse response) -> {
                        getViewState().hideProgress();
                        getViewState().setUserInfo(response.result);
                    },
                    (Throwable throwable) -> {
                        getViewState().hideProgress();
                        getViewState().updateUserError();
                    });
            return;
        }

        String partImage = AppUtils.getPath(mContext, imageUri);

        if (partImage != null) {
            File image = new File(partImage);
            RequestBody body = RequestBody.create(MediaType.parse("image/*"), image);
            imagePart = MultipartBody.Part.createFormData("avatar", image.getName(), body);
        }

        mApiService.updateUserInfo(userNamePart, phoneNumberPart, emailPart, imagePart).subscribe((UserResponse response) -> {
                    getViewState().hideProgress();
                    getViewState().setUserInfo(response.result);
                },
                (Throwable throwable) -> {
                    getViewState().hideProgress();
                    getViewState().updateUserError();
                });
    }
}

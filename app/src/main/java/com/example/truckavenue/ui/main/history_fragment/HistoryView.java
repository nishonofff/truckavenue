package com.example.truckavenue.ui.main.history_fragment;

import com.arellomobile.mvp.MvpView;
import com.example.truckavenue.data.network.entity.HistoryResponse;

import java.util.List;

public interface HistoryView extends MvpView {
    void showNoInternetConnection();

    void setHistoryList(List<HistoryResponse.History> historyList);

    void errorInLoading();

    void showProgress();

    void hideProgress();
}

package com.example.truckavenue.ui.parking.add_comment;

import android.annotation.SuppressLint;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.truckavenue.App;
import com.example.truckavenue.data.network.ApiService;
import com.example.truckavenue.data.network.entity.CommentResponse;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

@InjectViewState
public class AddCommentPresenter extends MvpPresenter<AddCommentView> {

    private static final String TAG = AddCommentPresenter.class.getName();

    @Inject
    ApiService mApiService;

    AddCommentPresenter() {
        App.getAppComponent().inject(this);
    }

    @SuppressLint("CheckResult")
    void addComment(int parkingId, String comment, String rate) {
        if (mApiService.noConnection()) {
            getViewState().showNoInternetConnection();
            return;
        }

        getViewState().showProgress();

        Map<String, Object> body = new HashMap<>();
        body.put("parking", parkingId);
        body.put("comment", comment);
        body.put("rate", rate);

        mApiService.addComment(body).subscribe((CommentResponse response) -> {
                    getViewState().hideProgress();
                    getViewState().addSuccess();
                },
                (Throwable throwable) -> {
                    getViewState().hideProgress();
                    getViewState().addError();
                });
    }

}

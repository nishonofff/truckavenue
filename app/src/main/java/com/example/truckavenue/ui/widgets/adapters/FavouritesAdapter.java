package com.example.truckavenue.ui.widgets.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.truckavenue.R;
import com.example.truckavenue.data.network.entity.FavouriteListResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FavouritesAdapter extends RecyclerView.Adapter<FavouritesAdapter.ViewHolder> {

    private Context mContext;

    private static FavouriteItemClickListener mItemClickListener;
    private List<FavouriteListResponse.Favourite> mFavouriteList;

    public FavouritesAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void setItemClickListener(FavouriteItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }

    public void setFavouriteList(List<FavouriteListResponse.Favourite> favouriteList) {
        mFavouriteList = favouriteList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.favourites_item, parent, false), mItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.setFavouriteItem(mFavouriteList.get(i));
    }

    @Override
    public int getItemCount() {
        if (mFavouriteList != null)
            return mFavouriteList.size();
        else return 0;
    }

    public interface FavouriteItemClickListener {
        void onFavouriteItemClick(int position, FavouriteListResponse.Favourite favouriteItem);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.parking_name)
        TextView mParkingName;
        @BindView(R.id.parking_address)
        TextView mParkingAddress;

        FavouriteItemClickListener mItemClickListener;

        public ViewHolder(@NonNull View itemView, FavouriteItemClickListener itemClickListener) {
            super(itemView);
            mItemClickListener = itemClickListener;
            ButterKnife.bind(this, itemView);

        }

        void setFavouriteItem(FavouriteListResponse.Favourite favouriteItem) {
            mParkingName.setText(favouriteItem.parking.title);
            mParkingAddress.setText(favouriteItem.parking.address);
        }

        @OnClick(R.id.favourite_item)
        void onFavouriteItemClicked() {
            mItemClickListener.onFavouriteItemClick(getAdapterPosition(), mFavouriteList.get(getAdapterPosition()));
        }
    }


}

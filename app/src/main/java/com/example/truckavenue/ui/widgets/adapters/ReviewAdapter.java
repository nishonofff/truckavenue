package com.example.truckavenue.ui.widgets.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.truckavenue.R;
import com.example.truckavenue.data.network.entity.ReviewResponse;
import com.example.truckavenue.utils.AppUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.ViewHolder> {

    private Context mContext;

    private static ReviewItemClickListener mItemClickListener;
    private List<ReviewResponse.Review> mReviewList;

    public ReviewAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void setItemClickListener(ReviewItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }

    public void setReviewList(List<ReviewResponse.Review> reviewList) {
        mReviewList = reviewList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.review_item, parent, false), mItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.setReviewItem(mReviewList.get(i));
    }

    @Override
    public int getItemCount() {
        if (mReviewList != null)
            return mReviewList.size();
        else return 0;
    }

    public interface ReviewItemClickListener {

    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.review_sender)
        TextView mReviewSender;
        @BindView(R.id.review_date)
        TextView mReviewDate;
        @BindView(R.id.review_description)
        TextView mReviewDescription;
        @BindView(R.id.review_rating_bar)
        RatingBar mReviewRatingbar;

        ReviewItemClickListener mItemClickListener;

        public ViewHolder(@NonNull View itemView, ReviewItemClickListener itemClickListener) {
            super(itemView);
            mItemClickListener = itemClickListener;
            ButterKnife.bind(this, itemView);

        }

        void setReviewItem(ReviewResponse.Review reviewItem) {
            mReviewSender.setText(reviewItem.user.username);
            mReviewDate.setText(AppUtils.formatDateToLocal(reviewItem.created));
            mReviewDescription.setText(reviewItem.comment);
            mReviewRatingbar.setRating(reviewItem.rate);
        }
    }


}

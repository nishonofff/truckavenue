package com.example.truckavenue.ui.notifications;

import android.annotation.SuppressLint;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.truckavenue.App;
import com.example.truckavenue.data.network.ApiService;
import com.example.truckavenue.data.network.entity.NotificationResponse;

import javax.inject.Inject;

@InjectViewState
public class NotificationsPresenter extends MvpPresenter<NotificationsView> {

    private static final String TAG = NotificationsPresenter.class.getName();

    @Inject
    ApiService mApiService;

    NotificationsPresenter() {
        App.getAppComponent().inject(this);
    }

    @SuppressLint("CheckResult")
    void getNotificationList() {
        if (mApiService.noConnection()) {
            getViewState().showNoInternetConnection();
            return;
        }

        getViewState().showProgress();

        mApiService.getNotificationList().subscribe((NotificationResponse response) -> {
                    getViewState().hideProgress();
                    getViewState().setNotifications(response.result.data);
                },
                (Throwable throwable) -> {
                    getViewState().hideProgress();
                    getViewState().getNotificationsError();
                });


    }

}

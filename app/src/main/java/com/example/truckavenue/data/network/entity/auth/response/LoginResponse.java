package com.example.truckavenue.data.network.entity.auth.response;

import com.example.truckavenue.data.network.entity.BaseResponse;
import com.squareup.moshi.Json;

public class LoginResponse extends BaseResponse {
    @Json(name = "result")
    public Result result;

    public static class Result {
        @Json(name = "detail")
        public String detail;
    }
}

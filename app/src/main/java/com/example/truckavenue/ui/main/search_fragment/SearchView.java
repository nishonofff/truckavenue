package com.example.truckavenue.ui.main.search_fragment;

import com.arellomobile.mvp.MvpView;
import com.example.truckavenue.data.network.entity.ParkingResponse;

import java.util.List;

public interface SearchView extends MvpView {
    void showNoInternetConnection();

    void setParkings(List<ParkingResponse.Parking> parkingList);

    void errorInLoading();

    void showProgress();

    void hideProgress();
}

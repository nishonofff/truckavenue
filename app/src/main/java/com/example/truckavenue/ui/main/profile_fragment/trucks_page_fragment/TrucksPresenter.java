package com.example.truckavenue.ui.main.profile_fragment.trucks_page_fragment;

import android.annotation.SuppressLint;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.truckavenue.App;
import com.example.truckavenue.data.network.ApiService;
import com.example.truckavenue.data.network.entity.truck.TruckResponse;

import javax.inject.Inject;

@InjectViewState
public class TrucksPresenter extends MvpPresenter<TrucksView> {
    private static final String TAG = TrucksPresenter.class.getName();

    @Inject
    ApiService mApiService;

    TrucksPresenter() {
        App.getAppComponent().inject(this);
    }

    @SuppressLint("CheckResult")
    void getTrucksList() {
        if (mApiService.noConnection()) {
            getViewState().showNoInternetConnection();
            return;
        }
        getViewState().showProgress();

        mApiService.getTruckList().subscribe((TruckResponse response) -> {
                    getViewState().hideProgress();
                    getViewState().setTruckList(response.result.data);
                },
                (Throwable throwable) -> {
                    getViewState().hideProgress();
                });
    }

    @SuppressLint("CheckResult")
    void deleteTruck(int truckId) {
        if (mApiService.noConnection()) {
            getViewState().showNoInternetConnection();
            return;
        }
        getViewState().showProgress();

        mApiService.deleteTruck(truckId).subscribe((TruckResponse response) -> {
                    getViewState().hideProgress();
                    getViewState().truckDeleteSuccess();
                },
                (Throwable throwable) -> {
                    getViewState().hideProgress();
                    getViewState().errorInLoading();
                });
    }
}

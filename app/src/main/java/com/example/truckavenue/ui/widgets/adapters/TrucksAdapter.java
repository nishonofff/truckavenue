package com.example.truckavenue.ui.widgets.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.truckavenue.R;
import com.example.truckavenue.data.network.entity.truck.TruckResponse;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.truckavenue.utils.AppConstants.BASE_URL_PATH;

public class TrucksAdapter extends RecyclerView.Adapter<TrucksAdapter.ViewHolder> {

    private Context mContext;

    private static TruckItemClickListener mItemClickListener;
    private List<TruckResponse.Truck> mTrucksList;


    public TrucksAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void setItemClickListener(TruckItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }

    public void setTrucksList(List<TruckResponse.Truck> trucksList) {
        mTrucksList = trucksList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.truck_item, parent, false), mItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.setTruckItem(mTrucksList.get(i));
    }

    @Override
    public int getItemCount() {
        if (mTrucksList != null)
            return mTrucksList.size();
        else return 0;
    }

    public interface TruckItemClickListener {
        void onTruckUpdateClick(int position, TruckResponse.Truck truckItem);

        void onTruckDeleteClick(int position, TruckResponse.Truck truckItem);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.truck_model)
        TextView mTruckModel;
        @BindView(R.id.truck_number)
        TextView mTruckNumber;
        @BindView(R.id.truck_image_view)
        RoundedImageView mTruckImage;
        @BindView(R.id.ic_more)
        ImageView mMoreButton;

        TruckItemClickListener mItemClickListener;

        public ViewHolder(@NonNull View itemView, TruckItemClickListener itemClickListener) {
            super(itemView);
            mItemClickListener = itemClickListener;
            ButterKnife.bind(this, itemView);
        }

        public void setTruckItem(TruckResponse.Truck truckItem) {
            mTruckModel.setText(truckItem.model);
            mTruckNumber.setText(truckItem.number);

            Picasso.with(mContext)
                    .load(BASE_URL_PATH + truckItem.photo)
                    .into(mTruckImage);
        }

        @OnClick(R.id.ic_more)
        void onMoreButtonClicked() {
            PopupMenu popup = new PopupMenu(mContext, mMoreButton, Gravity.BOTTOM);
            popup.inflate(R.menu.truck_item_menu);
            popup.setOnMenuItemClickListener(item -> {
                switch (item.getItemId()) {
                    case R.id.update:
                        mItemClickListener.onTruckUpdateClick(getAdapterPosition(), mTrucksList.get(getAdapterPosition()));
                        break;
                    case R.id.delete:
                        mItemClickListener.onTruckDeleteClick(getAdapterPosition(), mTrucksList.get(getAdapterPosition()));
                        break;
                }
                return false;
            });
            popup.show();
        }
    }
}

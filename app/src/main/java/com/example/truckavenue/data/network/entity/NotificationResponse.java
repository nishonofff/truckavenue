package com.example.truckavenue.data.network.entity;

import com.squareup.moshi.Json;

import java.util.List;

public class NotificationResponse extends BaseResponse {

    @Json(name = "result")
    public Result result;

    public static class Result {
        @Json(name = "notifications")
        public List<Notification> data = null;
    }

    public static class Notification {
        public String id;
        public String photo;
        public String title;
        public String description;
        public String created;
    }
}

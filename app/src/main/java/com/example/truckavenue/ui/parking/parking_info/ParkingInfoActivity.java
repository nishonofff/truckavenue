package com.example.truckavenue.ui.parking.parking_info;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Fade;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.truckavenue.R;
import com.example.truckavenue.data.network.entity.ParkingResponse;
import com.example.truckavenue.ui.booking.BookingActivity;
import com.example.truckavenue.ui.parking.add_comment.AddCommentActivity;
import com.example.truckavenue.ui.parking.reviews.ReviewsActivity;
import com.example.truckavenue.ui.widgets.adapters.ImageAdapter;
import com.example.truckavenue.utils.AppConstants;
import com.example.truckavenue.utils.AppUtils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.Serializable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ParkingInfoActivity extends AppCompatActivity implements OnMapReadyCallback,
        ImageAdapter.ImageItemClickListener, RatingBar.OnRatingBarChangeListener, AppConstants {

    @BindView(R.id.map_view)
    MapView mMapView;

    @BindView(R.id.image_recycler_view)
    RecyclerView mImageRecyclerView;

    @BindView(R.id.add_review)
    RatingBar mRatingBar;

    @BindView(R.id.parking_title)
    TextView mParkingTitle;

    @BindView(R.id.parking_address)
    TextView mParkingAddress;

    @BindView(R.id.parking_description)
    TextView mParkingDescription;

    @BindView(R.id.parking_rating_bar)
    RatingBar mParkingRatingBar;

    @BindView(R.id.parking_working_hours)
    TextView mParkingWorkingHours;

    @BindView(R.id.parking_available_places)
    TextView mParkingAvailablePlaces;

    @BindView(R.id.parking_price)
    TextView mParkingPrice;

    @BindView(R.id.comment_user_name)
    TextView mCommentUserName;

    @BindView(R.id.comment_created_date)
    TextView mCommentCreatedDate;

    @BindView(R.id.comment_text)
    TextView mCommentText;

    @BindView(R.id.review_amount)
    TextView mReviewAmountView;

    @BindView(R.id.comment_rating_bar)
    RatingBar mCommentRatingBar;

    @BindView(R.id.reserve_button)
    Button mReserveButton;

    private ImageAdapter mImageAdapter;

    private GoogleMap map;
    private ParkingResponse.Parking mParkingItem;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parking_info);

        // Gets the MapView from the XML layout and creates it
        ButterKnife.bind(this);
        mMapView.onCreate(savedInstanceState);

        // Gets to GoogleMap from the MapView and does initialization stuff
        initGoogleMap(savedInstanceState);

        Fade fade = new Fade();
        View decor = getWindow().getDecorView();
        fade.excludeTarget(decor.findViewById(R.id.toolbar), true);
        fade.excludeTarget(android.R.id.statusBarBackground, true);
        fade.excludeTarget(android.R.id.navigationBarBackground, true);

        getWindow().setEnterTransition(fade);
        getWindow().setExitTransition(fade);

        mParkingItem = (ParkingResponse.Parking) getIntent().getSerializableExtra(AppConstants.KEY_PARKING_ITEM);
        setParkingDetails(mParkingItem);
        setImageAdapter();
        mRatingBar.setOnRatingBarChangeListener(this);
    }

    private void setImageAdapter() {
        if (mParkingItem.gallery.size() == 0) {
            mImageRecyclerView.setVisibility(View.GONE);
            return;
        }
        mImageAdapter = new ImageAdapter(this);
        mImageAdapter.setItemClickListener(this);
        mImageAdapter.setImageList(mParkingItem.gallery);
        mImageRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mImageRecyclerView.setAdapter(mImageAdapter);
    }

    @OnClick(R.id.toolbar_back_button)
    void onToolbarBackClicked() {
        onBackPressed();
    }

    private void initGoogleMap(Bundle savedInstanceState) {
        // *** IMPORTANT ***
        // MapView requires that the Bundle you pass contain _ONLY_ MapView SDK
        // objects or sub-Bundles.
        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAP_VIEW_BUNDLE_KEY);
        }
        mMapView.onCreate(mapViewBundle);
        mMapView.getMapAsync(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Bundle mapViewBundle = outState.getBundle(MAP_VIEW_BUNDLE_KEY);
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(MAP_VIEW_BUNDLE_KEY, mapViewBundle);
        }
        mMapView.onSaveInstanceState(mapViewBundle);
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        mMapView.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        mMapView.onStop();
    }

    @Override
    public void onMapReady(GoogleMap map) {

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

            LatLng park = new LatLng(mParkingItem.latitude, mParkingItem.longitude);
            map.addMarker(new MarkerOptions()
                    .position(park)
                    .icon(AppUtils.getBitmapDescriptorFromVector(this, R.drawable.ic_marker))
                    .title(mParkingItem.title));

            CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(mParkingItem.latitude, mParkingItem.longitude)).zoom(12).build();
            map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            // Zoom in the Google Map
            return;
        }
        map.setMyLocationEnabled(true);
    }

    @Override
    public void onPause() {
        mMapView.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mMapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @OnClick(R.id.reserve_button)
    void onReserveClicked() {
        Intent intent = new Intent(this, BookingActivity.class);
        intent.putExtra(KEY_PARKING_ITEM, (Serializable) mParkingItem);
        startActivity(intent);
    }

    @OnClick(R.id.add_review)
    void onAddReviewClicked() {
        Intent intent = new Intent(this, AddCommentActivity.class);
        intent.putExtra(KEY_PARKING_ID, mParkingItem.id);
        startActivity(intent);
    }

    @OnClick(R.id.all_reviews)
    void onAllReviewsClicked() {
        Intent intent = new Intent(this, ReviewsActivity.class);
        intent.putExtra(KEY_PARKING_ID, mParkingItem.id);
        startActivity(intent);
    }


    @Override
    public void onImageItemClick(int position, View view) {
        Intent intent = new Intent(this, ShowImageActivity.class);
        intent.putExtra(PARKING_IMAGE, mParkingItem.gallery.get(position).photo);
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, view, ViewCompat.getTransitionName(view));
        startActivity(intent, options.toBundle());
    }

    @Override
    public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
        Intent intent = new Intent(this, AddCommentActivity.class);
        intent.putExtra(KEY_PARKING_ID, mParkingItem.id);
        startActivity(intent);
    }

    @SuppressLint("DefaultLocale")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    void setParkingDetails(ParkingResponse.Parking parkingItem) {
        mParkingTitle.setText(parkingItem.title);
        mParkingAddress.setText(parkingItem.address);
        mParkingDescription.setText(parkingItem.description);

        mParkingWorkingHours.setText(parkingItem.workingTime);
        mParkingPrice.setText(String.format("%s $", parkingItem.prices.get(0).price));
        mParkingAvailablePlaces.setText(String.format("%d available places", parkingItem.freePlaces));

        mCommentUserName.setText(parkingItem.comments.user.userName);
        mCommentCreatedDate.setText(AppUtils.formatDateToLocal(parkingItem.comments.createdDate));
        mCommentText.setText(parkingItem.comments.comment);
        mCommentRatingBar.setRating(Float.parseFloat(parkingItem.comments.rate));

        if (parkingItem.freePlaces == 0) {
            mReserveButton.setBackground(getDrawable(R.drawable.btn_gradient_secondary));
            mReserveButton.setClickable(false);
        } else {
            mReserveButton.setBackground(getDrawable(R.drawable.btn_gradient_primary));
            mReserveButton.setClickable(true);
        }
    }
}

package com.example.truckavenue.data.network.entity.truck;

import com.squareup.moshi.Json;

import java.io.Serializable;
import java.util.List;

public class TruckResponse implements Serializable {

    @Json(name = "result")
    public Result result;

    public static class Result implements Serializable {
        @Json(name = "trucks")
        public List<Truck> data = null;
    }

    public static class Truck implements Serializable {
        public int id;
        @Json(name = "truck_number")
        public String number;
        @Json(name = "model")
        public String model;
        @Json(name = "photo")
        public String photo;
        @Json(name = "is_active")
        public Boolean isMain;
    }
}

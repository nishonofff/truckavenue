package com.example.truckavenue.ui.splash_screen;

import android.annotation.SuppressLint;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.truckavenue.App;
import com.example.truckavenue.data.network.ApiService;
import com.example.truckavenue.data.prefs.PrefsService;

import javax.inject.Inject;

@InjectViewState
public class SplashScreenPresenter extends MvpPresenter<SplashScreenView> {
    private static final String TAG = SplashScreenPresenter.class.getName();

    @Inject
    ApiService mApiService;

    @Inject
    PrefsService mPrefsService;

    SplashScreenPresenter() {
        App.getAppComponent().inject(this);
    }


    @SuppressLint("CheckResult")
    void checkUserStatus() {
        if (mApiService.noConnection()) {
            getViewState().showNoInternetConnection();
            Log.w(TAG, "No internet connection");
            return;
        }

        if (mPrefsService.getToken("token").equals("0"))
            getViewState().setUserStatus(false);

        else
            getViewState().setUserStatus(true);
    }

}

package com.example.truckavenue.di;

import com.example.truckavenue.App;
import com.example.truckavenue.di.module.AppModule;
import com.example.truckavenue.ui.add_payment.AddPaymentPresenter;
import com.example.truckavenue.ui.add_truck.AddTruckPresenter;
import com.example.truckavenue.ui.auth.AuthPresenter;
import com.example.truckavenue.ui.booking.BookingPresenter;
import com.example.truckavenue.ui.main.history_fragment.HistoryPresenter;
import com.example.truckavenue.ui.main.profile_fragment.ProfilePresenter;
import com.example.truckavenue.ui.main.profile_fragment.favourites_page_fragment.FavouritesPresenter;
import com.example.truckavenue.ui.main.profile_fragment.payment_page_fragment.PaymentPresenter;
import com.example.truckavenue.ui.main.profile_fragment.trucks_page_fragment.TrucksPresenter;
import com.example.truckavenue.ui.main.reservations_fragment.ReservationsPresenter;
import com.example.truckavenue.ui.main.search_fragment.SearchPresenter;
import com.example.truckavenue.ui.notifications.NotificationsPresenter;
import com.example.truckavenue.ui.parking.add_comment.AddCommentPresenter;
import com.example.truckavenue.ui.parking.find_parking.FindParkingPresenter;
import com.example.truckavenue.ui.parking.reviews.ReviewsPresenter;
import com.example.truckavenue.ui.settings.SettingsPresenter;
import com.example.truckavenue.ui.splash_screen.SplashScreenPresenter;
import com.example.truckavenue.ui.update_user_details.UpdateUserPresenter;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */

@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {

    void inject(App app);

    void inject(AuthPresenter presenter);

    void inject(AddTruckPresenter presenter);

    void inject(AddPaymentPresenter presenter);

    void inject(FavouritesPresenter presenter);

    void inject(PaymentPresenter presenter);

    void inject(TrucksPresenter presenter);

    void inject(SearchPresenter presenter);

    void inject(AddCommentPresenter presenter);

    void inject(NotificationsPresenter presenter);

    void inject(SplashScreenPresenter presenter);

    void inject(HistoryPresenter presenter);

    void inject(ReservationsPresenter presenter);

    void inject(BookingPresenter presenter);

    void inject(SettingsPresenter presenter);

    void inject(FindParkingPresenter presenter);

    void inject(ProfilePresenter presenter);

    void inject(UpdateUserPresenter presenter);

    void inject(ReviewsPresenter presenter);

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder app(App app);

        AppComponent build();
    }
}

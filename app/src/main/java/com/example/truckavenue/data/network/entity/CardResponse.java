package com.example.truckavenue.data.network.entity;

import com.squareup.moshi.Json;

public class CardResponse extends BaseResponse {

    @Json(name = "result")
    public Result result;

    public static class Result {
        String id;
        @Json(name = "cardholder_name")
        String cardholderName;
        String number;
        @Json(name = "exp_month")
        String expireMonth;
        @Json(name = "exp_year")
        String expireYear;
        String cvv;
    }
}

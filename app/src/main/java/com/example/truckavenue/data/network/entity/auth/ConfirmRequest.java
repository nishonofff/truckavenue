package com.example.truckavenue.data.network.entity.auth;

public class ConfirmRequest {
    public String phone;
    public String code;

    @Override
    public String toString() {
        return "{" +
                '\"'+"phone"+'\"'+":" + '\"'+ phone + '\"' +","+
                '\"'+"code"+'\"'+":" + '\"'+ code + '\"' +
                '}';
    }
}

package com.example.truckavenue.data.network.entity;

import com.squareup.moshi.Json;

import java.util.List;

public class CommentResponse {

    @Json(name = "result")
    public Result result;

    public static class Result {
        String id;
        String parking;
        String comment;
        String rate;
    }
}

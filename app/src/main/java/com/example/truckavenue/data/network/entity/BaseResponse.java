package com.example.truckavenue.data.network.entity;

import com.squareup.moshi.Json;

public class BaseResponse {
    @Json(name = "status")
    public int status;

    @Json(name = "error")
    public Error error;

    public static class Error {
        @Json(name = "message")
        public String message;
    }
}

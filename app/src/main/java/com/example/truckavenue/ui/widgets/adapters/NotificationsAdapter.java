package com.example.truckavenue.ui.widgets.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.truckavenue.R;
import com.example.truckavenue.data.network.entity.NotificationResponse;
import com.example.truckavenue.utils.AppUtils;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.truckavenue.utils.AppConstants.BASE_URL_PATH;

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.ViewHolder> {

    private Context mContext;

    private static NotificationItemClickListener mItemClickListener;
    List<NotificationResponse.Notification> mNotificationsList;

    public void setItemClickListener(NotificationItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }

    public NotificationsAdapter(Context context) {
        mContext = context;
    }

    public void setNotificationsList(List<NotificationResponse.Notification> notificationsList) {
        mNotificationsList = notificationsList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_item, parent, false), mItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.setNotificationItem(mNotificationsList.get(i));
    }

    @Override
    public int getItemCount() {
        if (mNotificationsList != null)
            return mNotificationsList.size();
        else return 0;
    }

    public interface NotificationItemClickListener {
        void onNotificationItemClick(int position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.notification_date)
        TextView mNotificationDate;
        @BindView(R.id.notification_title)
        TextView mNotificationTitle;
        @BindView(R.id.notification_description)
        TextView mNotificationDescription;
        @BindView(R.id.notification_image)
        RoundedImageView mNotificationImage;

        private NotificationItemClickListener mItemClickListener;

        public ViewHolder(@NonNull View itemView, NotificationItemClickListener itemClickListener) {
            super(itemView);
            mItemClickListener = itemClickListener;
            ButterKnife.bind(this, itemView);
        }

        public void setNotificationItem(NotificationResponse.Notification item) {

            mNotificationDate.setText(item.created);
            mNotificationTitle.setText(item.title);
            mNotificationDescription.setText(item.description);
            mNotificationDate.setText(AppUtils.formatDateToLocal(item.created));

            Picasso.with(mContext)
                    .load(BASE_URL_PATH + item.photo)
                    .into(mNotificationImage);
        }

        @OnClick(R.id.notification_item)
        void onNotificationItemClicked() {
            if (mNotificationDescription.getVisibility() == View.VISIBLE) {
                mNotificationDescription.setVisibility(View.GONE);
                notifyItemChanged(getAdapterPosition());
            } else {
                mNotificationDescription.setVisibility(View.VISIBLE);
            }
        }
    }

}

package com.example.truckavenue.ui.main.history_fragment;

import android.annotation.SuppressLint;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.truckavenue.App;
import com.example.truckavenue.data.network.ApiService;
import com.example.truckavenue.data.network.entity.HistoryResponse;

import javax.inject.Inject;

@InjectViewState
public class HistoryPresenter extends MvpPresenter<HistoryView> {

    private static final String TAG = HistoryPresenter.class.getName();

    @Inject
    ApiService mApiService;

    HistoryPresenter() {
        App.getAppComponent().inject(this);
    }

    @SuppressLint("CheckResult")
    void getHistoryList() {
        if (mApiService.noConnection()) {
            getViewState().showNoInternetConnection();
            return;
        }
        getViewState().showProgress();

        mApiService.getHistoryList().subscribe((HistoryResponse response) -> {
                    getViewState().hideProgress();
                    getViewState().setHistoryList(response.result.data);
                },
                (Throwable throwable) -> {
                    getViewState().hideProgress();
                    getViewState().errorInLoading();
                });
    }
}

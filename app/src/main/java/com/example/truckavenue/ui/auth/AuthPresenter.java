package com.example.truckavenue.ui.auth;

import android.annotation.SuppressLint;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.truckavenue.App;
import com.example.truckavenue.data.network.ApiService;
import com.example.truckavenue.data.network.entity.auth.LoginRequest;
import com.example.truckavenue.data.network.entity.auth.response.ConfirmResponse;
import com.example.truckavenue.data.network.entity.auth.response.LoginResponse;
import com.example.truckavenue.data.network.entity.auth.response.RegisterResponse;
import com.example.truckavenue.data.prefs.PrefsService;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, May 24
 */

@InjectViewState
public class AuthPresenter extends MvpPresenter<AuthView> {

    private static final String TAG = AuthPresenter.class.getName();

    @Inject
    ApiService mApiService;

    @Inject
    PrefsService mPrefsService;

    AuthPresenter() {
        App.getAppComponent().inject(this);
    }

    @SuppressLint("CheckResult")
    void onLogin(String phone) {
        if (mApiService.noConnection()) {
            getViewState().showNoInternetConnection();
            Log.w(TAG, "No internet connection");
            return;
        }
        getViewState().showProgress();

        LoginRequest body = new LoginRequest();
        body.phone = phone;

        mApiService.login(body).subscribe((LoginResponse response) -> {
            getViewState().hideProgress();
            if (response.error != null) {
                getViewState().userNotFoundError(response.error.message);
            } else getViewState().loginSuccess(response.result.detail);

        }, (Throwable throwable) -> {
            getViewState().userNotFoundError(throwable.getMessage());
        });

    }

    @SuppressLint("CheckResult")
    void onSignUp(String phone, String userName, String email) {
        if (mApiService.noConnection()) {
            getViewState().showNoInternetConnection();
            Log.w(TAG, "No internet connection");
            return;
        }
        getViewState().showProgress();

        Map<String, String> body = new HashMap<>();
        body.put("phone", phone);
        body.put("name", userName);
        body.put("email", email);

        mApiService.register(body).subscribe((RegisterResponse response) -> {
                    if (response.status == 409) {
                        getViewState().userAlreadyExistError(response.error.message);
                    } else {
                        getViewState().signUpSuccess(response.result.detail);
                    }
                },
                (Throwable throwable) -> getViewState().unexpectedError("Error"));

    }

    @SuppressLint("CheckResult")
    void onConfirm(String phone, String code) {
        if (mApiService.noConnection()) {
            getViewState().showNoInternetConnection();
            Log.w(TAG, "No internet connection");
            return;
        }
        getViewState().hideProgress();

        Map<String, String> body = new HashMap<>();
        body.put("phone", phone);
        body.put("code", code);

        mApiService.confirm(body).subscribe((ConfirmResponse response) -> {

                    getViewState().hideProgress();

                    if (response.error != null) {
                        getViewState().confirmCodeError(response.error.message);
                    } else {
                        mApiService.setAuthKeys(response.result.token);
                        mPrefsService.setToken("token", String.format("Token" + " " + response.result.token));

                        getViewState().confirmSuccess(response.result.token);
                    }
                },
                (Throwable throwable) -> getViewState().unexpectedError("Error"));
    }
}

package com.example.truckavenue.ui.auth.callback;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, May 24
 */

public interface RestoreFragmentCallback {

    void showRestoreProgress();

    void hideRestoreProgress();

    void onErrorPhoneNumber(String message);
}

package com.example.truckavenue.ui.parking.parking_info;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.transition.Fade;
import android.view.View;
import android.widget.ImageView;

import com.example.truckavenue.R;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.senab.photoview.PhotoViewAttacher;

import static com.example.truckavenue.ui.parking.parking_info.ParkingInfoActivity.PARKING_IMAGE;
import static com.example.truckavenue.utils.AppConstants.BASE_URL_PATH;

public class ShowImageActivity extends AppCompatActivity {

    @BindView(R.id.image_view)
    ImageView mImageView;

    private PhotoViewAttacher mViewAttacher;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_image);

        ButterKnife.bind(this);

        android.transition.Fade fade = new Fade();
        View decor = getWindow().getDecorView();
        fade.excludeTarget(android.R.id.statusBarBackground, true);
        fade.excludeTarget(android.R.id.navigationBarBackground, true);

        getWindow().setEnterTransition(fade);
        getWindow().setExitTransition(fade);

        String image = getIntent().getStringExtra(PARKING_IMAGE);

        Picasso.with(this)
                .load(BASE_URL_PATH + image)
                .into(mImageView);

        mViewAttacher=new PhotoViewAttacher(mImageView,true);
    }


    @OnClick(R.id.image_view_background)
    void onBckClicked() {
        onBackPressed();
    }

}

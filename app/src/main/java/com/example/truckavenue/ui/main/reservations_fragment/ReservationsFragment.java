package com.example.truckavenue.ui.main.reservations_fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.truckavenue.R;
import com.example.truckavenue.data.network.entity.HistoryResponse;
import com.example.truckavenue.ui.main.callback.MainActivityListener;
import com.example.truckavenue.ui.widgets.adapters.ReservationsAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */
public class ReservationsFragment extends MvpAppCompatFragment implements ReservationsView, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.reservations_recycler_view)
    RecyclerView mRecyclerView;
    @BindView(R.id.empty_view)
    View mEmptyView;
    @BindView(R.id.progress_bar)
    View mProgressView;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    private MainActivityListener mActivityListener;

    private ReservationsAdapter mAdapter;

    @InjectPresenter
    ReservationsPresenter mReservationsPresenter;

    public static ReservationsFragment newInstance() {
        ReservationsFragment fragment = new ReservationsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivityListener = ((MainActivityListener) context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mReservationsPresenter.getRservationsList();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_reservations, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);

        mAdapter = new ReservationsAdapter(getActivity());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        mSwipeRefreshLayout.setOnRefreshListener(this);
    }


    @OnClick(R.id.toolbar_notifications)
    void onNotificationClicked() {
        mActivityListener.onNotificationClicked();
    }

    @Override
    public void showNoInternetConnection() {

    }

    @Override
    public void loadSuccess() {

    }

    @Override
    public void setReservationsList(List<HistoryResponse.History> reservationsList) {
        if (reservationsList.size() == 0) {
            mEmptyView.setVisibility(View.VISIBLE);
        } else {
            mEmptyView.setVisibility(View.GONE);
        }
        mAdapter.setReservationList(reservationsList);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void errorInLoading() {

    }

    @Override
    public void showProgress() {
        mSwipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onRefresh() {
        mReservationsPresenter.getRservationsList();
    }
}

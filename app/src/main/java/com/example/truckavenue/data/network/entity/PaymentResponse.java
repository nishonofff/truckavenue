package com.example.truckavenue.data.network.entity;

import com.squareup.moshi.Json;

public class PaymentResponse extends BaseResponse {

    @Json(name = "result")
    public Result result;

    public static class Result {
        public String id;
        @Json(name = "order")
        public BookingResponse.Result order;
        @Json(name = "card")
        public CardListResponse.Card card;
    }

}

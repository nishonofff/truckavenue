package com.example.truckavenue.ui.main.profile_fragment.trucks_page_fragment;

import com.arellomobile.mvp.MvpView;
import com.example.truckavenue.data.network.entity.truck.TruckResponse;

import java.util.List;

public interface TrucksView extends MvpView {
    void showNoInternetConnection();

    void truckDeleteSuccess();

    void setTruckList(List<TruckResponse.Truck> truckList);

    void errorInLoading();

    void showProgress();

    void hideProgress();
}

package com.example.truckavenue.ui.booking;

import com.arellomobile.mvp.MvpView;
import com.example.truckavenue.data.network.entity.BookingResponse;
import com.example.truckavenue.data.network.entity.CardListResponse;

import java.util.List;

public interface BookingView extends MvpView {

    void showNoInternetConnection();

    void setPaymentList(List<CardListResponse.Card> cardList);

    void showProgress();

    void hideProgress();

    void bookingSuccess(BookingResponse.Result bookingResponse);

    void paymentSuccess();

    void bookingError(String message);

    void notFoundActiveTruckError(String message);

    void errorInLoading();

}

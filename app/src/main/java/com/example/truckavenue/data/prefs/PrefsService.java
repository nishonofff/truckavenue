package com.example.truckavenue.data.prefs;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */

public interface PrefsService {

    String LOCALE = "locale";

    void clearPrefs();

    String getLocale();

    void setLocale(String locale);

    String getToken(String key);

    void setToken(String key, String token);

    void clearUserData();

}

package com.example.truckavenue.ui.main.profile_fragment.favourites_page_fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.truckavenue.R;
import com.example.truckavenue.data.network.entity.FavouriteListResponse;
import com.example.truckavenue.ui.parking.parking_info.ParkingInfoActivity;
import com.example.truckavenue.ui.widgets.adapters.FavouritesAdapter;
import com.example.truckavenue.ui.widgets.dialogs.AlertDialogs;
import com.example.truckavenue.utils.AppConstants;

import java.io.Serializable;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */
public class FavouritesFragment extends MvpAppCompatFragment implements FavouritesView,
        FavouritesAdapter.FavouriteItemClickListener, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.favourites_recycler_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.progress_bar)
    View mProgressView;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    private FavouritesAdapter mAdapter;

    @InjectPresenter
    FavouritesPresenter mFavouritesPresenter;

    public static FavouritesFragment newInstance() {
        FavouritesFragment fragment = new FavouritesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFavouritesPresenter.getFavouritesList();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_page_favourites, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);

        mSwipeRefreshLayout.setOnRefreshListener(this);

        mAdapter = new FavouritesAdapter(getActivity());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    @Override
    public void showNoInternetConnection() {
        AlertDialogs.showNoConnectionMessage(getActivity(),
                (dialog, which) -> dialog.dismiss(),
                (dialog, which) -> dialog.dismiss());
    }

    @Override
    public void setFavouriteList(List<FavouriteListResponse.Favourite> favouriteList) {
        mAdapter.setItemClickListener(this);
        mAdapter.setFavouriteList(favouriteList);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void errorInLoading() {
        AlertDialogs.showErrorDialog(getActivity());
    }

    @Override
    public void showProgress() {
        mSwipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onFavouriteItemClick(int position, FavouriteListResponse.Favourite favouriteItem) {
        Intent intent = new Intent(getActivity(), ParkingInfoActivity.class);
        intent.putExtra(AppConstants.KEY_PARKING_ITEM, (Serializable) favouriteItem.parking);
        startActivity(intent);
    }

    @Override
    public void onRefresh() {
        mFavouritesPresenter.getFavouritesList();
    }
}

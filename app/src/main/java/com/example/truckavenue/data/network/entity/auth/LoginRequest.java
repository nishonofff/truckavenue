package com.example.truckavenue.data.network.entity.auth;

public class LoginRequest {
    public String phone;

    @Override
    public String toString() {
        return "{" +
                '\"'+"phone"+'\"'+":" + '\"'+ phone + '\"' +
                '}';
    }
}

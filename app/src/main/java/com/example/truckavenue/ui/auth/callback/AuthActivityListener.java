package com.example.truckavenue.ui.auth.callback;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, May 24
 */

public interface AuthActivityListener {

    void setLoginVisibility(boolean visibility);

    void setSyncVisibility(boolean visibility);

    void onForgotPassword();

    void onLogin(LoginFragmentCallback loginCallback, String phoneNumber);

    void onSignUp(LoginFragmentCallback loginCallback, String phoneNumber, String userName, String email);

    void onConfirmation(ConfirmFragmentCallback confirmCallback, String confirmCode);

    void onRestorePassword(RestoreFragmentCallback restoreCallback, String phoneNumber);

    void onResendConfirmCode();

    void onMain();
}

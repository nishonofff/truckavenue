package com.example.truckavenue.utils;

public interface AppConstants {
    /**
     * Network timeouts
     */
    byte CONNECT_TIMEOUT = 120;
    byte WRITE_TIMEOUT = 120;
    byte READ_TIMEOUT = 120;

    String BASE_API_URL = "https://tweak.group/truck_avenue/api/";

    String BASE_URL_PATH = "https://tweak.group/";

    String PREFS_FILE_NAME = "prefs";

    String KEY_PARKING_ID = "PARKING_ID";
    String KEY_PARKING_ITEM = "PARKING_ITEM";
    String MAP_VIEW_BUNDLE_KEY = "MapViewBundleKey";
    String PARKING_IMAGE = "PARKING_IMAGE";


    String INTENT_TYPE_ADD_TRUCK = "INTENT_TYPE_ADD_TRUCK";
    String INTENT_TYPE_ADD = "ADD";
    String INTENT_TYPE_UPDATE = "UPDATE";
    String KEY_TRUCK_ITEM = "TRUCK_ITEM";
    String INTENT_TYPE_ADD_FIRST = "INTENT_TYPE_ADD_FIRST";
}

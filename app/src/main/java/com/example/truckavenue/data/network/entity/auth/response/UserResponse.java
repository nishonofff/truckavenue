package com.example.truckavenue.data.network.entity.auth.response;

import com.example.truckavenue.data.network.entity.BaseResponse;
import com.squareup.moshi.Json;

public class UserResponse extends BaseResponse {

    @Json(name = "result")
    public Result result;

    public static class Result {
        @Json(name = "id")
        public String id;
        @Json(name = "avatar")
        public String avatar;
        @Json(name = "username")
        public String userName;
        @Json(name = "phone_number")
        public String phoneNumber;
        @Json(name = "email")
        public String email;
        public int token = -1;
    }
}

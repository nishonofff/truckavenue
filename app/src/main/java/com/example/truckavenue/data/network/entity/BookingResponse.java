package com.example.truckavenue.data.network.entity;

import com.example.truckavenue.data.network.entity.truck.TruckResponse;
import com.squareup.moshi.Json;

public class BookingResponse extends BaseResponse {

    @Json(name = "result")
    public Result result;

    public static class Result {
        public int id;
        @Json(name = "parking")
        public ParkingResponse.Parking parking;
        @Json(name = "user_truck")
        public TruckResponse.Truck truck;
        @Json(name = "from_date")
        public String startDate;
        @Json(name = "to_date")
        public String endDate;
        @Json(name = "total_price")
        public long totalPrice;
        @Json(name = "created")
        public String createdDate;
    }

}

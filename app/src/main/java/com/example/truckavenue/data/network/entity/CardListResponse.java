package com.example.truckavenue.data.network.entity;

import com.squareup.moshi.Json;

import java.util.List;

public class CardListResponse extends BaseResponse {

    @Json(name = "result")
    public Result result;

    public static class Result {
        @Json(name = "cards")
        public List<Card> cardList = null;
    }

    public static class Card{
        public int id;
        public String type;
        @Json(name = "cardholder_name")
        public String cardholderName;
        public String number;
        @Json(name = "exp_month")
        public String expireMonth;
        @Json(name = "exp_year")
        public String expireYear;
        public String cvv;
        @Json(name = "card_id")
        public String cardID;
        @Json(name = "toke")
        public String token;
        @Json(name = "client_ip")
        public String clientIP;
    }

}

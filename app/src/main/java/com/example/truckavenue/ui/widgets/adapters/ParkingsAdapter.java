package com.example.truckavenue.ui.widgets.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.truckavenue.R;
import com.example.truckavenue.data.network.entity.ParkingResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ParkingsAdapter extends RecyclerView.Adapter<ParkingsAdapter.ViewHolder> {

    private Context mContext;

    private static ParkingItemClickListener mItemClickListener;
    private List<ParkingResponse.Parking> mParkingList;

    public ParkingsAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void setItemClickListener(ParkingItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }

    public void setParkingList(List<ParkingResponse.Parking> parkingList) {
        mParkingList = parkingList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.parking_item, parent, false), mItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.setFavouriteItem(mParkingList.get(i));
    }

    @Override
    public int getItemCount() {
        if (mParkingList != null)
            return mParkingList.size();
        else return 0;
    }

    public interface ParkingItemClickListener {
        void onParkingItemClick(int position, ParkingResponse.Parking parkingItem);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.parking_title)
        TextView mParkingTitle;
        @BindView(R.id.parking_address)
        TextView mParkingAddress;
        @BindView(R.id.parking_rating_bar)
        RatingBar mParkingRatingBar;

        ParkingItemClickListener mItemClickListener;

        public ViewHolder(@NonNull View itemView, ParkingItemClickListener itemClickListener) {
            super(itemView);
            mItemClickListener = itemClickListener;
            ButterKnife.bind(this, itemView);
        }

        public void setFavouriteItem(ParkingResponse.Parking parkingItem) {
            mParkingTitle.setText(parkingItem.title);
            mParkingAddress.setText(parkingItem.address);
//            mParkingRatingBar.setNumStars(parkingItem.maximumRate);
            mParkingRatingBar.setRating(parkingItem.rate);
        }

        @OnClick(R.id.parking_item)
        void onParkingItemClicked() {
            mItemClickListener.onParkingItemClick(getAdapterPosition(), mParkingList.get(getAdapterPosition()));
        }
    }


}

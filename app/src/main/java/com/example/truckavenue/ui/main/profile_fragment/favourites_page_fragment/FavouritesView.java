package com.example.truckavenue.ui.main.profile_fragment.favourites_page_fragment;

import com.arellomobile.mvp.MvpView;
import com.example.truckavenue.data.network.entity.FavouriteListResponse;

import java.util.List;

public interface FavouritesView extends MvpView {

    void showNoInternetConnection();

    void setFavouriteList(List<FavouriteListResponse.Favourite> favouriteList);

    void errorInLoading();

    void showProgress();

    void hideProgress();
}

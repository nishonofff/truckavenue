package com.example.truckavenue.ui.main.search_fragment;

import android.annotation.SuppressLint;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.truckavenue.App;
import com.example.truckavenue.data.network.ApiService;
import com.example.truckavenue.data.network.entity.ParkingResponse;

import javax.inject.Inject;

@InjectViewState
public class SearchPresenter extends MvpPresenter<SearchView> {
    private static final String TAG = SearchPresenter.class.getName();

    @Inject
    ApiService mApiService;

    SearchPresenter() {
        App.getAppComponent().inject(this);
    }

    @SuppressLint("CheckResult")
    void getParkingList() {
        if (mApiService.noConnection()) {
            getViewState().showNoInternetConnection();
            return;
        }
        getViewState().showProgress();

        mApiService.getParkingList().subscribe((ParkingResponse response) -> {
                    getViewState().setParkings(response.result.data);
                    getViewState().hideProgress();
                },
                (Throwable throwable) -> {
                    getViewState().errorInLoading();
                    getViewState().hideProgress();
                });

    }
}

package com.example.truckavenue.ui.auth;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.truckavenue.R;
import com.example.truckavenue.ui.add_truck.AddTruckActivity;
import com.example.truckavenue.ui.auth.callback.AuthActivityListener;
import com.example.truckavenue.ui.auth.callback.ConfirmFragmentCallback;
import com.example.truckavenue.ui.auth.callback.LoginFragmentCallback;
import com.example.truckavenue.ui.auth.callback.RestoreFragmentCallback;
import com.example.truckavenue.ui.auth.confirm_fragment.ConfirmFragment;
import com.example.truckavenue.ui.auth.login_fragment.LoginFragment;
import com.example.truckavenue.ui.main.MainActivity;
import com.example.truckavenue.ui.widgets.dialogs.AlertDialogs;

import static com.example.truckavenue.utils.AppConstants.INTENT_TYPE_ADD_FIRST;
import static com.example.truckavenue.utils.AppConstants.INTENT_TYPE_ADD_TRUCK;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, May 24
 */

public class AuthActivity extends MvpAppCompatActivity implements AuthActivityListener, AuthView {

    private FragmentManager mFragmentManager;

    @InjectPresenter
    AuthPresenter mAuthPresenter;

    private String mPhoneNumber;

    private boolean mIsNewUser = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        mFragmentManager = getSupportFragmentManager();

        mFragmentManager.beginTransaction()
                .replace(R.id.container, LoginFragment.newInstance())
                .addToBackStack(LoginFragment.TAG)
                .commit();
    }

    @Override
    public void setLoginVisibility(boolean visibility) {

    }

    @Override
    public void setSyncVisibility(boolean visibility) {

    }

    @Override
    public void onForgotPassword() {

    }

    @Override
    public void onLogin(LoginFragmentCallback loginCallback, String phoneNumber) {
        mAuthPresenter.onLogin(phoneNumber);
        mPhoneNumber = phoneNumber;
    }

    @Override
    public void onSignUp(LoginFragmentCallback loginCallback, String phoneNumber, String userName, String email) {
        mAuthPresenter.onSignUp(phoneNumber, userName, email);
        mPhoneNumber = phoneNumber;
    }

    @Override
    public void onConfirmation(ConfirmFragmentCallback confirmCallback, String confirmCode) {
        mAuthPresenter.onConfirm(mPhoneNumber, confirmCode);
    }

    @Override
    public void onRestorePassword(RestoreFragmentCallback restoreCallback, String phoneNumber) {

    }

    @Override
    public void onResendConfirmCode() {
        mAuthPresenter.onLogin(mPhoneNumber);
    }

    @Override
    public void onMain() {

    }

    @Override
    public void showNoInternetConnection() {
        AlertDialogs.showNoConnectionMessage(this,
                (dialog, which) -> dialog.dismiss(),
                (dialog, which) -> dialog.dismiss());
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void loginSuccess(String message) {
        mIsNewUser = false;

        mFragmentManager.beginTransaction()
                .replace(R.id.container, ConfirmFragment.newInstance())
                .addToBackStack(LoginFragment.TAG)
                .commit();
    }

    @Override
    public void userNotFoundError(String message) {
        AlertDialogs.showErrorDialog(this, message);
    }

    @Override
    public void incorrectPasswordError(String message) {

    }

    @Override
    public void unexpectedError(String message) {
    }

    @Override
    public void confirmSuccess(String message) {
        Intent intent;

        if (mIsNewUser) {
            intent = new Intent(this, AddTruckActivity.class);
            intent.putExtra(INTENT_TYPE_ADD_TRUCK, INTENT_TYPE_ADD_FIRST);
        } else {
            intent = new Intent(this, MainActivity.class);
        }

        startActivity(intent);
    }

    @Override
    public void confirmCodeError(String message) {
        AlertDialogs.showErrorDialog(this, message);
    }

    @Override
    public void signUpSuccess(String message) {
        mIsNewUser = true;

        mFragmentManager.beginTransaction()
                .replace(R.id.container, ConfirmFragment.newInstance())
                .addToBackStack(LoginFragment.TAG)
                .commit();
    }

    @Override
    public void userAlreadyExistError(String message) {
        AlertDialogs.showErrorDialog(this, message);
    }
}

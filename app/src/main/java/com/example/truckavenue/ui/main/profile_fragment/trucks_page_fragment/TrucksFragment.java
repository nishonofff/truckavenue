package com.example.truckavenue.ui.main.profile_fragment.trucks_page_fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.truckavenue.R;
import com.example.truckavenue.data.network.entity.truck.TruckResponse;
import com.example.truckavenue.ui.add_truck.AddTruckActivity;
import com.example.truckavenue.ui.widgets.adapters.TrucksAdapter;
import com.example.truckavenue.ui.widgets.dialogs.AlertDialogs;
import com.example.truckavenue.utils.AppConstants;

import java.io.Serializable;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */
public class TrucksFragment extends MvpAppCompatFragment implements TrucksView,
        TrucksAdapter.TruckItemClickListener, SwipeRefreshLayout.OnRefreshListener, AppConstants {

    @BindView(R.id.trucks_recycler_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    @BindView(R.id.empty_view)
    View mEmptyView;

    @BindView(R.id.progress_bar)
    View mProgressView;

    @InjectPresenter
    TrucksPresenter mTrucksPresenter;

    private TrucksAdapter mAdapter;

    private List<TruckResponse.Truck> mTrucksList;

    public static TrucksFragment newInstance() {
        TrucksFragment fragment = new TrucksFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        mTrucksPresenter.getTrucksList();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_page_trucks, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);

        mSwipeRefreshLayout.setOnRefreshListener(this);
        setUpRecyclerView();
    }

    private void setUpRecyclerView() {
        mAdapter = new TrucksAdapter(getActivity());
        mAdapter.setItemClickListener(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(mAdapter);
    }

    @OnClick(R.id.add_truck_button)
    void onAddTruckClicked() {
        Intent intent = new Intent(getActivity(), AddTruckActivity.class);
        intent.putExtra(INTENT_TYPE_ADD_TRUCK, INTENT_TYPE_ADD);
        startActivity(intent);
    }

    @Override
    public void setTruckList(List<TruckResponse.Truck> truckList) {
        mTrucksList = truckList;
        if (truckList.size() == 0) {
            mEmptyView.setVisibility(View.VISIBLE);
        } else {
            mEmptyView.setVisibility(View.GONE);
        }

        mAdapter.setTrucksList(truckList);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void showNoInternetConnection() {
        AlertDialogs.showNoConnectionMessage(getActivity(),
                (dialog, which) -> dialog.dismiss(),
                (dialog, which) -> dialog.dismiss());
    }

    @Override
    public void truckDeleteSuccess() {
        mTrucksPresenter.getTrucksList();
    }

    @Override
    public void errorInLoading() {
        AlertDialogs.showErrorDialog(getActivity());
    }

    @Override
    public void showProgress() {
        mSwipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onTruckUpdateClick(int position, TruckResponse.Truck truckItem) {
        Intent intent = new Intent(getActivity(), AddTruckActivity.class);
        intent.putExtra(KEY_TRUCK_ITEM, (Serializable) mTrucksList.get(position));
        intent.putExtra(INTENT_TYPE_ADD_TRUCK, INTENT_TYPE_UPDATE);
        startActivity(intent);
    }

    @Override
    public void onTruckDeleteClick(int position, TruckResponse.Truck truckItem) {
        mTrucksPresenter.deleteTruck(truckItem.id);
    }

    @Override
    public void onRefresh() {
        mTrucksPresenter.getTrucksList();
    }
}

package com.example.truckavenue.ui.notifications;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.truckavenue.R;
import com.example.truckavenue.data.network.entity.NotificationResponse;
import com.example.truckavenue.ui.widgets.adapters.NotificationsAdapter;
import com.example.truckavenue.ui.widgets.dialogs.AlertDialogs;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NotificationsActivity extends MvpAppCompatActivity implements NotificationsView, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.notifications_recycler_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    @BindView(R.id.progress_bar)
    View mProgressView;

    @BindView(R.id.empty_view)
    View mEmptyView;

    @InjectPresenter
    NotificationsPresenter mNotificationsPresenter;

    private NotificationsAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);

        ButterKnife.bind(this);

        mAdapter = new NotificationsAdapter(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mNotificationsPresenter.getNotificationList();

        mSwipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @OnClick(R.id.toolbar_back_button)
    void onToolbarBackClicked() {
        onBackPressed();
    }

    @Override
    public void showNoInternetConnection() {

    }

    @Override
    public void setNotifications(List<NotificationResponse.Notification> notificationsList) {
        if (notificationsList.size() == 0)
            mEmptyView.setVisibility(View.VISIBLE);
        else
            mEmptyView.setVisibility(View.GONE);

        mAdapter.setNotificationsList(notificationsList);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void getNotificationsError() {
        AlertDialogs.showErrorDialog(this);
    }

    @Override
    public void showProgress() {
        mSwipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onRefresh() {
        mNotificationsPresenter.getNotificationList();
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}

package com.example.truckavenue.data.network;

import com.example.truckavenue.data.network.entity.BookingResponse;
import com.example.truckavenue.data.network.entity.CardListResponse;
import com.example.truckavenue.data.network.entity.CardResponse;
import com.example.truckavenue.data.network.entity.CommentResponse;
import com.example.truckavenue.data.network.entity.FavouriteListResponse;
import com.example.truckavenue.data.network.entity.HistoryResponse;
import com.example.truckavenue.data.network.entity.NotificationResponse;
import com.example.truckavenue.data.network.entity.ParkingResponse;
import com.example.truckavenue.data.network.entity.PaymentResponse;
import com.example.truckavenue.data.network.entity.ReviewResponse;
import com.example.truckavenue.data.network.entity.auth.LoginRequest;
import com.example.truckavenue.data.network.entity.auth.response.ConfirmResponse;
import com.example.truckavenue.data.network.entity.auth.response.LoginResponse;
import com.example.truckavenue.data.network.entity.auth.response.RegisterResponse;
import com.example.truckavenue.data.network.entity.auth.response.UserResponse;
import com.example.truckavenue.data.network.entity.truck.TruckModelResponse;
import com.example.truckavenue.data.network.entity.truck.TruckResponse;

import java.util.Map;

import io.reactivex.Single;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */

public interface TweakAPI {

    @Headers("Content-Type: application/json")
    @POST("user/register")
    Single<RegisterResponse> registerUser(@Body Map<String, String> body);

    @Headers("Content-Type: application/json")
    @POST("user/login")
    Single<LoginResponse> login(@Body LoginRequest body);

    @Headers("Content-Type: application/json")
    @POST("user/confirm")
    Single<ConfirmResponse> loginConfirm(@Body Map<String, String> body);

    @Multipart
    @Headers("Accept: application/json")
    @PUT("user/update")
    Single<UserResponse> updateUserInfo(@Header("Authorization") String accessToken,
                                        @Part("username") RequestBody username,
                                        @Part("phone_number") RequestBody phoneNumber,
                                        @Part("email") RequestBody email,
                                        @Part MultipartBody.Part avatar);

    @Multipart
    @Headers("Accept: application/json")
    @PUT("user/update")
    Single<UserResponse> updateUserInfo(@Header("Authorization") String accessToken,
                                        @Part("username") RequestBody username,
                                        @Part("phone_number") RequestBody phoneNumber,
                                        @Part("email") RequestBody email);

    @GET("user/detail")
    Single<UserResponse> getUserInfo(@Header("Authorization") String accessToken);

    @Headers("Content-Type: application/json")
    @POST("parking/order/create")
    Single<BookingResponse> addBooking(@Header("Authorization") String accessToken, @Body Map<String, Object> body);

    @Headers("Content-Type: application/json")
    @POST("user/payment/paid")
    Single<PaymentResponse> makePayment(@Header("Authorization") String accessToken, @Body Map<String, Integer> body);

    @Multipart
    @Headers("Accept: application/json")
    @POST("user/truck/")
    Single<TruckResponse> addTruck(
            @Header("Authorization") String accessToken,
            @Part("truck_number") RequestBody truckNumber,
            @Part("model") RequestBody truckModel,
            @Part MultipartBody.Part truckImage
    );

    @Multipart
    @Headers("Accept: application/json")
    @POST("user/truck/")
    Single<TruckResponse> addTruck(
            @Header("Authorization") String accessToken,
            @Part("truck_number") RequestBody truckNumber,
            @Part("model") RequestBody truckModel,
            @Part MultipartBody.Part truckImage,
            @Part("is_active") Boolean isActive
    );

    @Multipart
    @Headers("Accept: application/json")
    @PUT("user/truck/{truckId}/")
    Single<TruckResponse> updateTruck(
            @Path("truckId") int truckId,
            @Header("Authorization") String accessToken,
            @Part("truck_number") RequestBody truckNumber,
            @Part("model") RequestBody truckModel,
            @Part("is_active") Boolean isActive
    );

    @Multipart
    @Headers("Accept: application/json")
    @PUT("user/truck/{truckId}/")
    Single<TruckResponse> updateTruck(
            @Path("truckId") int truckId,
            @Header("Authorization") String accessToken,
            @Part("truck_number") RequestBody truckNumber,
            @Part("model") RequestBody truckModel,
            @Part MultipartBody.Part truckImage,
            @Part("is_active") Boolean isActive
    );

    @Headers("Accept: application/json")
    @DELETE("user/truck/{truckId}/")
    Single<TruckResponse> deleteTruck(
            @Header("Authorization") String accessToken,
            @Path("truckId") int truckId
    );


    @GET("user/truck")
    Single<TruckResponse> getTruckList(@Header("Authorization") String accessToken);

    @GET("trucks")
    Single<TruckModelResponse> getTruckModelList(@Header("Authorization") String accessToken);

    @GET("parking/list")
    Single<ParkingResponse> getParkingList(@Header("Authorization") String accessToken);

    @GET("notifications")
    Single<NotificationResponse> getNotificationList(@Header("Authorization") String accessToken);

    @GET("user/favorite/")
    Single<FavouriteListResponse> getFavouriteList(@Header("Authorization") String accessToken);

    @GET("user/order/history/all/list")
    Single<HistoryResponse> getHistoryList(@Header("Authorization") String accessToken);

    @GET("user/order/history/all/list")
    Single<HistoryResponse> getReservationList(@Header("Authorization") String accessToken);

    @POST("parking/comment/")
    Single<CommentResponse> addComment(@Header("Authorization") String accessToken, @Body Map<String, Object> body);

    @DELETE("user/favorite/")
    Single<ConfirmResponse> getFavouriteItem(@Field("Token") String accessToken);

    @PUT("user/favorite/")
    Single<ConfirmResponse> addToFavouriteList(@Field("Token") String accessToken);

    @Headers("Content-Type: application/json")
    @POST("user/payment/add/visa")
    Single<CardResponse> addCard(@Header("Authorization") String accessToken, @Body Map<String, String> body);

    @Headers("Content-Type: application/json")
    @DELETE("user/payment/card/delete/{cardId}")
    Single<CardResponse> deleteCard(@Header("Authorization") String accessToken, @Path("cardId") int cardId);

    @GET("user/payment/card/list")
    Single<CardListResponse> getCardList(@Header("Authorization") String accessToken);

    @Headers("Content-Type: application/json")
    @GET("parking/comment/{parkingId}/get_parking/")
    Single<ReviewResponse> getAllReviews(@Header("Authorization") String accessToken, @Path("parkingId") int parkingId);

}

package com.example.truckavenue.ui.auth;

import com.arellomobile.mvp.MvpView;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, May 24
 */

interface AuthView extends MvpView {

    void showNoInternetConnection();

    void showProgress();

    void hideProgress();

    void loginSuccess(String message);

    void userNotFoundError(String message);

    void incorrectPasswordError(String message);

    void unexpectedError(String message);

    void confirmSuccess(String message);

    void confirmCodeError(String message);

    void signUpSuccess(String message);

    void userAlreadyExistError(String message);
}

package com.example.truckavenue.ui.add_truck;

import com.arellomobile.mvp.MvpView;

public interface AddTruckView extends MvpView {

    void showNoInternetConnection();

    void addSuccess();

    void addError();

    void showProgress();

    void hideProgress();

    void setModelList(String[] modelList);


}

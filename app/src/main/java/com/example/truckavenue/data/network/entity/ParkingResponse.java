package com.example.truckavenue.data.network.entity;

import com.squareup.moshi.Json;

import java.io.Serializable;
import java.util.List;

public class ParkingResponse extends BaseResponse implements Serializable{

    @Json(name = "result")
    public Result result;

    public static class Result implements Serializable {
        @Json(name = "parking")
        public List<Parking> data = null;
    }

    public static class Parking implements Serializable {
        public int id;
        @Json(name = "title")
        public String title;
        @Json(name = "description")
        public String description;
        @Json(name = "address")
        public String address;
        @Json(name = "longitude")
        public double longitude;
        @Json(name = "latitude")
        public double latitude;
        @Json(name = "working_time")
        public String workingTime;
        @Json(name = "free_places")
        public int freePlaces;
        @Json(name = "our_tax")
        public String ourTax;
        @Json(name = "logo")
        public String logo;
        @Json(name = "photo")
        public String photo;
        @Json(name = "phones")
        public List<Phone> phones = null;
        @Json(name = "prices")
        public List<Price> prices = null;
        @Json(name = "comments")
        public Comment comments;
        @Json(name = "gallery")
        public List<Photo> gallery = null;
        @Json(name = "stars")
        public int rate;
        @Json(name = "stars_len")
        public double maximumRate;
        @Json(name = "favorite")
        public Favourite favourite;
    }

    public static class Phone implements Serializable {
        public String id;
        @Json(name = "title")
        public String title;
        @Json(name = "phone")
        public String phone;
    }

    public static class Price implements Serializable {
        public String id;
        @Json(name = "price")
        public long price;
    }

    public static class Comment implements Serializable {
        @Json(name = "user")
        public User user;
        @Json(name = "comment")
        public String comment;
        @Json(name = "created")
        public String createdDate;
        @Json(name = "rate")
        public String rate;
    }

    public static class Photo implements Serializable {
        public String id;
        @Json(name = "photo")
        public String photo;
    }

    public static class User implements Serializable {
        String id;
        @Json(name = "username")
        public String userName;
        @Json(name = "email")
        public String email;
    }

    public static class Favourite implements Serializable {
        String id;
        @Json(name = "is_favorite")
        public boolean isFavourite;
    }
}

package com.example.truckavenue.data.network.entity;

import com.example.truckavenue.data.network.entity.truck.TruckResponse;
import com.squareup.moshi.Json;

import java.io.Serializable;
import java.util.List;

public class HistoryResponse extends BaseResponse implements Serializable {

    @Json(name = "result")
    public Result result;

    public static class Result implements Serializable {
        @Json(name = "history")
        public List<History> data = null;
    }

    public static class History implements Serializable {
        public String id;
        @Json(name = "from_date")
        public String startDate;
        @Json(name = "to_date")
        public String endDate;
        @Json(name = "created")
        public String created;
        @Json(name = "total_price")
        public String totalPrice;

        @Json(name = "parking")
        public ParkingResponse.Parking parking;
        @Json(name = "user_truck")
        public TruckResponse.Truck truck;

        @Json(name = "status")
        public String status;
    }
}

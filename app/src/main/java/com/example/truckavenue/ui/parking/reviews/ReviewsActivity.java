package com.example.truckavenue.ui.parking.reviews;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.truckavenue.R;
import com.example.truckavenue.data.network.entity.ReviewResponse;
import com.example.truckavenue.ui.parking.parking_info.ParkingInfoActivity;
import com.example.truckavenue.ui.widgets.adapters.ReviewAdapter;
import com.example.truckavenue.ui.widgets.dialogs.AlertDialogs;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ReviewsActivity extends MvpAppCompatActivity implements ReviewsView,
        SwipeRefreshLayout.OnRefreshListener, ReviewAdapter.ReviewItemClickListener {

    @BindView(R.id.reviews_recycler_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    @InjectPresenter
    ReviewsPresenter mReviewsPresenter;

    private ReviewAdapter mReviewAdapter;

    private int mParkingID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reviews);

        ButterKnife.bind(this);

        mParkingID = getIntent().getIntExtra(ParkingInfoActivity.KEY_PARKING_ID, 0);

        setUpRecyclerView();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @OnClick(R.id.toolbar_back_button)
    void onToolbarBackClicked() {
        onBackPressed();
    }


    private void setUpRecyclerView() {
        mReviewAdapter = new ReviewAdapter(this);
        mReviewAdapter.setItemClickListener(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mReviewAdapter);
        mSwipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mReviewsPresenter.getAllReviews(mParkingID);
    }

    @Override
    public void showNoInternetConnection() {
        AlertDialogs.showNoConnectionMessage(this,
                (dialog, which) -> {
                    mReviewsPresenter.getAllReviews(mParkingID);
                },
                (dialog, which) -> dialog.dismiss());
    }

    @Override
    public void setReviewList(List<ReviewResponse.Review> reviewList) {
        mReviewAdapter.setReviewList(reviewList);
        mReviewAdapter.notifyDataSetChanged();
    }

    @Override
    public void loadParkingListError() {
        AlertDialogs.showErrorDialog(this);
    }

    @Override
    public void showProgress() {
        mSwipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onRefresh() {
        mReviewsPresenter.getAllReviews(mParkingID);
    }


}

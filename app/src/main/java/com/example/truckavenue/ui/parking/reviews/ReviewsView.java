package com.example.truckavenue.ui.parking.reviews;

import com.arellomobile.mvp.MvpView;
import com.example.truckavenue.data.network.entity.ReviewResponse;

import java.util.List;

public interface ReviewsView extends MvpView {

    void showNoInternetConnection();

    void setReviewList(List<ReviewResponse.Review> reviewList);

    void loadParkingListError();

    void showProgress();

    void hideProgress();

}

package com.example.truckavenue.ui.widgets.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.truckavenue.R;
import com.example.truckavenue.data.network.entity.HistoryResponse;
import com.example.truckavenue.utils.AppUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {

    private Context mContext;

    private static HistoryItemClickListener mItemClickListener;
    private List<HistoryResponse.History> mHistoryList;

    public HistoryAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void setItemClickListener(HistoryItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }

    public void setHistoryList(List<HistoryResponse.History> historyList) {
        mHistoryList = historyList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.history_item, parent, false), mItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.setHistoryItem(mHistoryList.get(i));
    }

    @Override
    public int getItemCount() {
        if (mHistoryList != null)
            return mHistoryList.size();
        else return 0;
    }

    public interface HistoryItemClickListener {
        void onHistoryItemClick(int position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.history_end_date)
        TextView mHistoryEndDate;
        @BindView(R.id.history_parking)
        TextView mHistoryParking;
        @BindView(R.id.history_price)
        TextView mHistoryPrice;

        HistoryItemClickListener mItemClickListener;

        public ViewHolder(@NonNull View itemView, HistoryItemClickListener itemClickListener) {
            super(itemView);
            mItemClickListener = itemClickListener;
            ButterKnife.bind(this, itemView);

        }

        void setHistoryItem(HistoryResponse.History historyItem) {
            mHistoryEndDate.setText(AppUtils.formatDateToLocal(historyItem.created));
            mHistoryParking.setText(historyItem.parking.address);
            mHistoryPrice.setText(String.format("%s $", historyItem.totalPrice));
        }
    }


}

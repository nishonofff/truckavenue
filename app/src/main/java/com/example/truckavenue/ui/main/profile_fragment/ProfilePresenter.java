package com.example.truckavenue.ui.main.profile_fragment;

import android.annotation.SuppressLint;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.truckavenue.App;
import com.example.truckavenue.data.network.ApiService;
import com.example.truckavenue.data.network.entity.auth.response.UserResponse;

import javax.inject.Inject;

@InjectViewState
public class ProfilePresenter extends MvpPresenter<ProfileView> {
    private static final String TAG = ProfilePresenter.class.getName();

    @Inject
    ApiService mApiService;

    ProfilePresenter() {
        App.getAppComponent().inject(this);
    }

    @SuppressLint("CheckResult")
    void getUserInfo() {
        if (mApiService.noConnection()) {
            getViewState().showNoInternetConnection();
            return;
        }
        getViewState().showProgress();

        mApiService.getUserInfo().subscribe((UserResponse response) -> {
                    getViewState().hideProgress();
                    getViewState().setUserInfo(response.result);
                },
                (Throwable throwable) -> {
                    getViewState().hideProgress();
                    getViewState().errorInLoading();
                });
    }

}

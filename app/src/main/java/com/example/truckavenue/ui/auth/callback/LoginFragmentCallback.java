package com.example.truckavenue.ui.auth.callback;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, May 24
 */

public interface LoginFragmentCallback {

    void showLoginProgress();

    void hideLoginProgress();

    void onErrorUserName(String message);

    void onErrorEmail(String message);

    void onErrorPhoneNumber(String message);
}

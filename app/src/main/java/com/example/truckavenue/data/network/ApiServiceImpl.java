package com.example.truckavenue.data.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.example.truckavenue.data.network.entity.BookingResponse;
import com.example.truckavenue.data.network.entity.CardListResponse;
import com.example.truckavenue.data.network.entity.CardResponse;
import com.example.truckavenue.data.network.entity.CommentResponse;
import com.example.truckavenue.data.network.entity.FavouriteListResponse;
import com.example.truckavenue.data.network.entity.HistoryResponse;
import com.example.truckavenue.data.network.entity.NotificationResponse;
import com.example.truckavenue.data.network.entity.ParkingResponse;
import com.example.truckavenue.data.network.entity.PaymentResponse;
import com.example.truckavenue.data.network.entity.ReviewResponse;
import com.example.truckavenue.data.network.entity.auth.ConfirmRequest;
import com.example.truckavenue.data.network.entity.auth.LoginRequest;
import com.example.truckavenue.data.network.entity.auth.response.ConfirmResponse;
import com.example.truckavenue.data.network.entity.auth.response.LoginResponse;
import com.example.truckavenue.data.network.entity.auth.response.RegisterResponse;
import com.example.truckavenue.data.network.entity.auth.response.UserResponse;
import com.example.truckavenue.data.network.entity.truck.TruckModelResponse;
import com.example.truckavenue.data.network.entity.truck.TruckResponse;
import com.example.truckavenue.data.prefs.PrefsService;
import com.example.truckavenue.utils.AppConstants;

import java.util.Map;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */

public class ApiServiceImpl implements ApiService, AppConstants {

    private final Context mContext;
    private final TweakAPI mAPI;
    private final PrefsService mPrefsService;
    private String mLocale;
    private String mAccessToken;

    public ApiServiceImpl(Context context, TweakAPI API, PrefsService prefsService) {
        mContext = context;
        mAPI = API;
        mPrefsService = prefsService;
        mAccessToken = mPrefsService.getToken("token");
    }

    @Override
    public void setLocale(String locale) {
        mLocale = locale;
    }

    @Override
    public void setAuthKeys(String accessToken) {
        mAccessToken = String.format("Token" + " " + accessToken);
    }

    @Override
    public boolean noConnection() {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(
                Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm != null ? cm.getActiveNetworkInfo() : null;
        if (activeNetwork != null) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return !activeNetwork.isConnected();
            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return !activeNetwork.isConnected();
        }
        return true;
    }


    @Override
    public Single<RegisterResponse> register(Map<String, String> body) {
        return mAPI.registerUser(body)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Single<LoginResponse> login(LoginRequest body) {
        return mAPI.login(body)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Single<ConfirmResponse> confirm(Map<String, String> body) {
        return mAPI.loginConfirm(body)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Single<TruckResponse> getTruckList() {
        return mAPI.getTruckList(mAccessToken)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Single<TruckModelResponse> getTruckModelList() {
        return mAPI.getTruckModelList(mAccessToken)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Single<CardListResponse> getCardList() {
        return mAPI.getCardList(mAccessToken)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Single<ParkingResponse> getParkingList() {
        return mAPI.getParkingList(mAccessToken)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Single<NotificationResponse> getNotificationList() {
        return mAPI.getNotificationList(mAccessToken)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Single<CommentResponse> addComment(Map<String, Object> body) {
        return mAPI.addComment(mAccessToken, body)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Single<FavouriteListResponse> getFavouriteList() {
        return mAPI.getFavouriteList(mAccessToken)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Single<HistoryResponse> getHistoryList() {
        return mAPI.getHistoryList(mAccessToken)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Single<HistoryResponse> getReservationList() {
        return mAPI.getReservationList(mAccessToken)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Single<TruckResponse> addTruck(RequestBody truckNumber,
                                          RequestBody truckModel,
                                          MultipartBody.Part truckImage) {
        return mAPI.addTruck(mAccessToken, truckNumber, truckModel, truckImage)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Single<TruckResponse> addTruck(RequestBody truckNumber,
                                          RequestBody truckModel,
                                          MultipartBody.Part truckImage,
                                          Boolean isActive) {
        return mAPI.addTruck(mAccessToken, truckNumber, truckModel, truckImage, isActive)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Single<TruckResponse> updateTruck(int truckId,
                                             RequestBody truckNumber,
                                             RequestBody truckModel,
                                             Boolean isActive) {
        return mAPI.updateTruck(truckId, mAccessToken, truckNumber, truckModel, isActive)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }


    @Override
    public Single<TruckResponse> updateTruck(int truckId,
                                             RequestBody truckNumber,
                                             RequestBody truckModel,
                                             MultipartBody.Part truckImage,
                                             Boolean isActive) {
        return mAPI.updateTruck(truckId, mAccessToken, truckNumber, truckModel, truckImage, isActive)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Single<TruckResponse> deleteTruck(int truckId) {
        return mAPI.deleteTruck(mAccessToken, truckId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Single<ConfirmResponse> getFavouriteItem(ConfirmRequest body) {
        return null;
    }

    @Override
    public Single<UserResponse> getUserInfo() {
        return mAPI.getUserInfo(mAccessToken)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Single<UserResponse> updateUserInfo(RequestBody username,
                                               RequestBody phoneNumber,
                                               RequestBody email,
                                               MultipartBody.Part avatar) {
        return mAPI.updateUserInfo(mAccessToken, username, phoneNumber, email, avatar)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Single<UserResponse> updateUserInfo(RequestBody username, RequestBody phoneNumber, RequestBody email) {
        return mAPI.updateUserInfo(mAccessToken, username, phoneNumber, email)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Single<ConfirmResponse> addToFavouriteList(ConfirmRequest body) {
        return null;
    }

    @Override
    public Single<CardResponse> addCard(Map<String, String> body) {
        return mAPI.addCard(mAccessToken, body)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Single<CardResponse> deleteCard(int cardId) {
        return mAPI.deleteCard(mAccessToken, cardId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Single<BookingResponse> addBooking(Map<String, Object> body) {
        return mAPI.addBooking(mAccessToken, body)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Single<PaymentResponse> makePayment(Map<String, Integer> body) {
        return mAPI.makePayment(mAccessToken, body)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Single<ReviewResponse> getAllReviews(int parkingId) {
        return mAPI.getAllReviews(mAccessToken, parkingId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

}

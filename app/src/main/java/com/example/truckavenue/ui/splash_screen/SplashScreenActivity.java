package com.example.truckavenue.ui.splash_screen;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.truckavenue.R;
import com.example.truckavenue.ui.auth.AuthActivity;
import com.example.truckavenue.ui.main.MainActivity;
import com.example.truckavenue.ui.widgets.dialogs.AlertDialogs;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */
public class SplashScreenActivity extends MvpAppCompatActivity implements SplashScreenView {

    private SplashScreenActivity mContext;

    @InjectPresenter
    SplashScreenPresenter mPresenter;

    int SPLASH_SCREEN_DURATION = 2000;
    private Intent mIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        mContext = this;
        mPresenter.checkUserStatus();
    }

    @Override
    public void showNoInternetConnection() {
        AlertDialogs.showNoConnectionMessage(this,
                (dialog, which) -> mPresenter.checkUserStatus(),
                (dialog, which) -> dialog.dismiss());
    }

    @Override
    public void setUserStatus(boolean loggedIn) {
        if (loggedIn)
            mIntent = new Intent(mContext, MainActivity.class);
        else
            mIntent = new Intent(mContext, AuthActivity.class);

        mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

        new Handler().postDelayed(() -> {
            if (mIntent != null) {
                mContext.startActivity(mIntent);
            }
        }, SPLASH_SCREEN_DURATION);
    }
}

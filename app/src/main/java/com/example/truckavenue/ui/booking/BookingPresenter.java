package com.example.truckavenue.ui.booking;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.truckavenue.App;
import com.example.truckavenue.data.network.ApiService;
import com.example.truckavenue.data.network.entity.BookingResponse;
import com.example.truckavenue.data.network.entity.CardListResponse;
import com.example.truckavenue.data.network.entity.PaymentResponse;
import com.example.truckavenue.data.prefs.PrefsService;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

@InjectViewState
public class BookingPresenter extends MvpPresenter<BookingView> {

    private static final String TAG = BookingPresenter.class.getName();

    @Inject
    ApiService mApiService;

    @Inject
    PrefsService mPrefsService;

    BookingPresenter() {
        App.getAppComponent().inject(this);
    }

    @SuppressLint("CheckResult")
    void addBooking(int parkingId, String startDate, String endDate) {
        if (mApiService.noConnection()) {
            getViewState().showNoInternetConnection();
            Log.w(TAG, "No internet connection");
            return;
        }
        getViewState().showProgress();

        Map<String, Object> body = new HashMap<>();
        body.put("parking", parkingId);
        body.put("from_date", startDate);
        body.put("to_date", endDate);

        mApiService.addBooking(body).subscribe((BookingResponse response) -> {
            getViewState().hideProgress();
            getViewState().bookingSuccess(response.result);

        }, (Throwable throwable) -> {
            getViewState().hideProgress();
            getViewState().bookingError("Error!");
        });
    }

    @SuppressLint("CheckResult")
    void makePayment(Integer orderId, Integer cardId) {
        if (mApiService.noConnection()) {
            getViewState().showNoInternetConnection();
            Log.w(TAG, "No internet connection");
            return;
        }
        getViewState().showProgress();

        Map<String, Integer> body = new HashMap<>();
        body.put("order_id", orderId);
        body.put("card_id", cardId);

        mApiService.makePayment(body).subscribe((PaymentResponse response) -> {
            getViewState().hideProgress();
            if (response.status == 400) {
                getViewState().notFoundActiveTruckError(response.error.message);
            }

            getViewState().paymentSuccess();

        }, (Throwable throwable) -> {
            getViewState().hideProgress();
            getViewState().bookingError("Error!");
        });
    }

    @SuppressLint("CheckResult")
    void getPaymentsList() {
        if (mApiService.noConnection()) {
            getViewState().showNoInternetConnection();
            return;
        }
        getViewState().showProgress();

        mApiService.getCardList().subscribe((CardListResponse response) -> {
                    getViewState().hideProgress();
                    getViewState().setPaymentList(response.result.cardList);
                },
                (Throwable throwable) -> {
                    getViewState().hideProgress();
                    getViewState().errorInLoading();
                });
    }

}

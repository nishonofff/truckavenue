package com.example.truckavenue.di.module;

import android.app.Application;
import android.content.Context;

import com.example.truckavenue.App;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */

@Singleton
@Module(includes = {NetworkModule.class, PrefsModule.class})
public abstract class AppModule {

    /**
     * Provides the application context.
     */
    @Provides
    static Context provideAppContext(Application application) {
        return application.getApplicationContext();
    }

    @Binds
    @Singleton
    /*
     * Singleton annotation isn't necessary since Application instance is unique but is here for
     * convention. In general, providing Activity, Fragment, BroadcastReceiver, etc does not require
     * them to be scoped since they are the components being injected and their instance is unique.
     *
     * However, having a scope annotation makes the module easier to read. We wouldn't have to look
     * at what is being provided in order to understand its scope.
     */
    abstract Application application(App app);
}

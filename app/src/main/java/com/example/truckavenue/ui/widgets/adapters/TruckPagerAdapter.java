package com.example.truckavenue.ui.widgets.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.truckavenue.ui.main.profile_fragment.favourites_page_fragment.FavouritesFragment;
import com.example.truckavenue.ui.main.profile_fragment.payment_page_fragment.PaymentPageFragment;
import com.example.truckavenue.ui.main.profile_fragment.trucks_page_fragment.TrucksFragment;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */

public class TruckPagerAdapter extends FragmentPagerAdapter {

    public TruckPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return TrucksFragment.newInstance();
            case 1:
                return FavouritesFragment.newInstance();
            case 2:
                return PaymentPageFragment.newInstance();
            default:
                return TrucksFragment.newInstance();
        }
    }
}

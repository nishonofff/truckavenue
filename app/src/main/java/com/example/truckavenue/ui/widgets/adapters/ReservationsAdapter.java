package com.example.truckavenue.ui.widgets.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.truckavenue.R;
import com.example.truckavenue.data.network.entity.HistoryResponse;
import com.example.truckavenue.utils.AppUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReservationsAdapter extends RecyclerView.Adapter<ReservationsAdapter.ViewHolder> {

    private Context mContext;

    private static ReservationItemClickListener mItemClickListener;
    private List<HistoryResponse.History> mReservationList;

    public void setItemClickListener(ReservationItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }

    public ReservationsAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void setReservationList(List<HistoryResponse.History> reservationList) {
        mReservationList = reservationList;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.reservations_item, parent, false), mItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.setReservationItem(mReservationList.get(i));
    }

    @Override
    public int getItemCount() {
        if (mReservationList != null)
            return mReservationList.size();
        else return 0;
    }

    public interface ReservationItemClickListener {
        void onReservationItemClick(int position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.reservation_date)
        TextView mReservationDate;
        @BindView(R.id.total_days)
        TextView mReservationTotalDays;
        @BindView(R.id.reservation_address)
        TextView mReservationAddress;

        ReservationItemClickListener mItemClickListener;

        public ViewHolder(@NonNull View itemView, ReservationItemClickListener itemClickListener) {
            super(itemView);
            mItemClickListener = itemClickListener;
            ButterKnife.bind(this, itemView);
        }

        @SuppressLint("DefaultLocale")
        public void setReservationItem(HistoryResponse.History reservationItem) {
            mReservationDate.setText(String.format("%s - %s", AppUtils.formatDateToLocal(reservationItem.startDate), AppUtils.formatDateToLocal(reservationItem.endDate)));
            mReservationTotalDays.setText(String.format("%d days , $%s USD", AppUtils.getDifferenceOfDays(reservationItem.startDate, reservationItem.endDate), reservationItem.totalPrice));
            mReservationAddress.setText(reservationItem.parking.address);
        }
    }


}

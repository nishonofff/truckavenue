package com.example.truckavenue.ui.main.profile_fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.truckavenue.R;
import com.example.truckavenue.data.network.entity.auth.response.UserResponse;
import com.example.truckavenue.ui.main.callback.MainActivityListener;
import com.example.truckavenue.ui.settings.SettingsActivity;
import com.example.truckavenue.ui.update_user_details.UpdateUserActivity;
import com.example.truckavenue.ui.widgets.adapters.TruckPagerAdapter;
import com.example.truckavenue.ui.widgets.dialogs.AlertDialogs;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.truckavenue.utils.AppConstants.BASE_URL_PATH;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */
public class ProfileFragment extends MvpAppCompatFragment implements ViewPager.OnPageChangeListener, ProfileView {

    @BindView(R.id.tab_layout)
    TabLayout mTabLayout;

    @BindView(R.id.view_pager)
    ViewPager mViewPager;

    @BindView(R.id.user_name)
    TextView mUserName;

    @BindView(R.id.user_phone_number)
    TextView mUserPhoneNumber;

    @BindView(R.id.user_tokens)
    TextView mUserTokens;

    @BindView(R.id.user_tokens_view)
    View mUserTokensView;

    @BindView(R.id.profile_image_view)
    RoundedImageView mUserImage;

    private TruckPagerAdapter mAdapter;
    int mCurrentPage = 0;

    @InjectPresenter
    ProfilePresenter mProfilePresenter;

    private MainActivityListener mActivityListener;

    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivityListener = ((MainActivityListener) context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        mProfilePresenter.getUserInfo();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
        FragmentManager fragmentManager = getChildFragmentManager();
        mAdapter = new TruckPagerAdapter(fragmentManager);

        mViewPager.setAdapter(mAdapter);
        mTabLayout.setupWithViewPager(mViewPager);
        setTabLayoutIcons();
        mViewPager.addOnPageChangeListener(this);
        mTabLayout.getTabAt(0).select();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mTabLayout.getTabAt(0).select();
    }

    @OnClick(R.id.toolbar_notifications)
    void onNotificationClicked() {
        mActivityListener.onNotificationClicked();
    }

    @OnClick(R.id.toolbar_settings)
    void onSettingsClicked() {
        Intent intent = new Intent(getActivity(), SettingsActivity.class);
        startActivity(intent);
    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {
        mCurrentPage = i;
        setTabLayoutIcons();
    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }

    void setTabLayoutIcons() {
        for (int i = 0; i < 3; i++) {
            mTabLayout.getTabAt(i).setIcon(getTabIcon(i));
        }
        mTabLayout.getTabAt(mCurrentPage).setIcon(getSelectedTabIcon(mCurrentPage));
    }

    int getTabIcon(int position) {
        switch (position) {
            case 0:
                return R.drawable.ic_truck;
            case 1:
                return R.drawable.ic_favourite;
            case 2:
                return R.drawable.ic_payment;
            default:
                return R.drawable.ic_truck;
        }
    }

    int getSelectedTabIcon(int position) {
        switch (position) {
            case 0:
                return R.drawable.ic_truck_selected;
            case 1:
                return R.drawable.ic_favourite_selected;
            case 2:
                return R.drawable.ic_payment_selected;
            default:
                return R.drawable.ic_truck_selected;
        }
    }

    @Override
    public void showNoInternetConnection() {
        AlertDialogs.showNoConnectionMessage(getActivity(),
                (dialog, which) -> dialog.dismiss(),
                (dialog, which) -> dialog.dismiss());
    }

    @Override
    public void setUserInfo(UserResponse.Result userInfo) {
        mUserName.setText(userInfo.userName);
        mUserPhoneNumber.setText(userInfo.phoneNumber);

        Picasso.with(getActivity())
                .load(BASE_URL_PATH + userInfo.avatar)
                .resize(mUserImage.getMeasuredWidth(), mUserImage.getMeasuredHeight())
                .centerCrop()
                .placeholder(R.drawable.profile_image_placeholder)
                .into(mUserImage);
        if (userInfo.token == -1) {
            mUserTokensView.setVisibility(View.GONE);
        } else {
            mUserTokens.setText(String.valueOf(userInfo.token));
            mUserTokensView.setVisibility(View.VISIBLE);
        }
    }

    @OnClick({R.id.profile_image_view, R.id.user_name, R.id.user_phone_number})
    void onUserClicked() {
        startActivity(new Intent(getActivity(), UpdateUserActivity.class));
    }

    @Override
    public void errorInLoading() {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }
}



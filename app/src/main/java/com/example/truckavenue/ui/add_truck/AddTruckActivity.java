package com.example.truckavenue.ui.add_truck;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.truckavenue.R;
import com.example.truckavenue.data.network.entity.truck.TruckResponse;
import com.example.truckavenue.ui.main.MainActivity;
import com.example.truckavenue.ui.main.profile_fragment.trucks_page_fragment.TrucksFragment;
import com.example.truckavenue.ui.widgets.dialogs.AlertDialogs;
import com.example.truckavenue.utils.AppConstants;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddTruckActivity extends MvpAppCompatActivity implements AddTruckView, AppConstants {

    @BindView(R.id.truck_add_image_view)
    RoundedImageView mTruckImageView;

    @BindView(R.id.truck_number_edit_text)
    TextInputEditText mTruckNumberEditText;

    @BindView(R.id.truck_model_edit_text)
    TextInputEditText mTruckModelEditText;

    @BindView(R.id.truck_switch)
    Switch mTruckSwitch;

    @BindView(R.id.progress_bar)
    View mProgressView;

    @BindView(R.id.add_truck_button)
    Button mAddTruckButton;

    @InjectPresenter
    AddTruckPresenter mAddTruckPresenter;

    private String[] mModelList;
    private final int GALLERY_REQUEST_CODE = 1;
    private Uri mImageUri = null;
    private TruckResponse.Truck mTruckItem;
    private String mIntentType;

    private final int REQUEST_PERMISSION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_truck);

        ButterKnife.bind(this);

        mIntentType = getIntent().getStringExtra(INTENT_TYPE_ADD_TRUCK);
        mTruckItem = (TruckResponse.Truck) getIntent().getSerializableExtra(KEY_TRUCK_ITEM);
        if (mTruckItem != null) {

            mTruckNumberEditText.setText(mTruckItem.number);
            mTruckModelEditText.setText(mTruckItem.model);
            if (mTruckItem.isMain)
                mTruckSwitch.setChecked(true);

            Picasso.with(this)
                    .load(BASE_URL_PATH + mTruckItem.photo)
                    .into(mTruckImageView);
            mAddTruckButton.setText(getString(R.string.txt_update_truck_btn));
        } else
            mAddTruckButton.setText(getString(R.string.add_truck_text));

        mAddTruckPresenter.getTruckModelList();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @OnClick(R.id.toolbar_back_button)
    void onToolbarBackClicked() {
        onBackPressed();
    }

    @OnClick(R.id.truck_add_image_view)
    void onAddImageClicked() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_PERMISSION);

            return;
        }

        pickFromGallery();
    }

    private void pickFromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, GALLERY_REQUEST_CODE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK)
            switch (requestCode) {
                case GALLERY_REQUEST_CODE:
                    Uri selectedImage = data.getData();
                    mImageUri = selectedImage;
                    mTruckImageView.setImageURI(selectedImage);
                    break;
            }
    }

    @OnClick(R.id.truck_model_edit_text)
    void onTruckModelClicked() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.title_txt_model_choose_dialog));
        builder.setItems(mModelList, (dialog, which) -> mTruckModelEditText.setText(mModelList[which]));
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @OnClick(R.id.add_truck_button)
    void onAddTruckClicked() {
        String truckNumber = mTruckNumberEditText.getText().toString();
        if (truckNumber.isEmpty()) {
            mTruckNumberEditText.setError(getResources().getString(R.string.empty_msg_truck_number));
            return;
        } else
            mTruckNumberEditText.setError(null);

        String truckModel = mTruckModelEditText.getText().toString();
        if (truckModel.isEmpty()) {
            mTruckModelEditText.setError(getResources().getString(R.string.empty_msg_truck_model));
            return;
        } else
            mTruckModelEditText.setError(null);

        if (mIntentType.equals(TrucksFragment.INTENT_TYPE_ADD)) {
            if (mImageUri == null)
                return;
            mAddTruckPresenter.addTruck(this, truckNumber, truckModel, mImageUri, mTruckSwitch.isChecked());
        } else if (mIntentType.equals(INTENT_TYPE_ADD_FIRST)) {
            if (mImageUri == null)
                return;
            mAddTruckPresenter.addTruck(this, truckNumber, truckModel, mImageUri, mTruckSwitch.isChecked());
        } else if (mIntentType.equals(TrucksFragment.INTENT_TYPE_UPDATE))
            mAddTruckPresenter.updateTruck(this, mTruckItem.id, truckNumber, truckModel, mImageUri, mTruckSwitch.isChecked());
    }

    @Override
    public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted.
            } else {
                // User refused to grant permission.
            }
        }
    }

    @Override
    public void showNoInternetConnection() {
        AlertDialogs.showNoConnectionMessage(this,
                (dialog, which) -> dialog.dismiss(),
                (dialog, which) -> dialog.dismiss());
    }

    @Override
    public void addSuccess() {
        if (mIntentType.equals(INTENT_TYPE_ADD_FIRST)) {
            startActivity(new Intent(this, MainActivity.class));
        } else {
            AlertDialogs.showSuccessDialog(this, getResources().getString(R.string.msg_truck_added));
            onBackPressed();
        }
    }

    @Override
    public void addError() {
        AlertDialogs.showErrorDialog(this);
    }

    @Override
    public void showProgress() {
        mProgressView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mProgressView.setVisibility(View.GONE);
    }

    @Override
    public void setModelList(String[] modelList) {
        mModelList = modelList;
    }
}

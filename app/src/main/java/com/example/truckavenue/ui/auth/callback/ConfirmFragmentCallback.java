package com.example.truckavenue.ui.auth.callback;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, May 24
 */

public interface ConfirmFragmentCallback {

    void showConfirmProgress();

    void hideConfirmProgress();

    void onErrorConfirmCode(String message);
}

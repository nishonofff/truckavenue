package com.example.truckavenue.ui.widgets.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.truckavenue.R;
import com.example.truckavenue.data.network.entity.CardListResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PaymentsAdapter extends RecyclerView.Adapter<PaymentsAdapter.ViewHolder> {

    private Context mContext;

    private static PaymentItemClickListener mItemClickListener;
    private List<CardListResponse.Card> mCardList = null;

    public PaymentsAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void setItemClickListener(PaymentItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }

    public void setCardList(List<CardListResponse.Card> cardList) {
        mCardList = cardList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.payment_item, parent, false), mItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.setPaymentItem(mCardList.get(i));
    }

    @Override
    public int getItemCount() {
        if (mCardList != null)
            return mCardList.size();
        else return 0;
    }

    public interface PaymentItemClickListener {
        void onPaymentItemClick(int position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.card_name)
        TextView mCardName;

        @BindView(R.id.card_number)
        TextView mCardNumber;

        PaymentItemClickListener mItemClickListener;

        public ViewHolder(@NonNull View itemView, PaymentItemClickListener itemClickListener) {
            super(itemView);
            mItemClickListener = itemClickListener;
            ButterKnife.bind(this, itemView);
        }

        public void setPaymentItem(CardListResponse.Card cardItem) {
            mCardName.setText(cardItem.type);
            mCardNumber.setText(String.format("..%s", cardItem.number.substring(cardItem.number.length() - 4)));
        }

        @OnClick(R.id.payment_item)
        void onPaymentItemClicked() {
            mItemClickListener.onPaymentItemClick(getAdapterPosition());
        }
    }


}

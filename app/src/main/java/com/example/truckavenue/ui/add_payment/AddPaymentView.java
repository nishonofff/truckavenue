package com.example.truckavenue.ui.add_payment;

import com.arellomobile.mvp.MvpView;

public interface AddPaymentView extends MvpView {

    void showNoInternetConnection();

    void addSuccess();

    void addError();

    void showProgress();

    void hideProgress();


}

package com.example.truckavenue.ui.settings;

import android.content.Intent;
import android.os.Bundle;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.truckavenue.R;
import com.example.truckavenue.ui.auth.AuthActivity;
import com.example.truckavenue.ui.widgets.dialogs.AlertDialogs;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingsActivity extends MvpAppCompatActivity implements SettingsView {

    @InjectPresenter
    SettingsPresenter mSettingsPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        ButterKnife.bind(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @OnClick(R.id.toolbar_back_button)
    void onToolbarBackClicked() {
        onBackPressed();
    }

    @OnClick(R.id.log_out_button)
    void onLogOutClicked() {
        AlertDialogs.logOutMessage(this,
                (dialog, which) -> {
                    mSettingsPresenter.onLogOut();
                    startActivity(new Intent(this, AuthActivity.class));
                },
                (dialog, which) -> dialog.dismiss());
    }
}

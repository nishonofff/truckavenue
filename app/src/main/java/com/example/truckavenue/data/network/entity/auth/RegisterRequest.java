package com.example.truckavenue.data.network.entity.auth;

public class RegisterRequest {
    private String name;
    private String email;
    private String phone;

    public RegisterRequest(String phone, String name, String email) {
        this.phone = phone;
        this.name = name;
        this.email = email;
    }

//    @android.support.annotation.NonNull
//    @Override
//    public String toString() {
//        return "{" +
//                '\"' + "phone" + '\"' + ":" + '\"' + phone + '\"' + "," +
//                '\"' + "name" + '\"' + ":" + '\"' + name + '\"' + "," +
//                '\"' + "email" + '\"' + ":" + '\"' + email + '\"' +
//                '}';
//    }

}

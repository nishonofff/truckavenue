package com.example.truckavenue.ui.main.history_fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.truckavenue.R;
import com.example.truckavenue.data.network.entity.HistoryResponse;
import com.example.truckavenue.ui.main.callback.MainActivityListener;
import com.example.truckavenue.ui.widgets.adapters.HistoryAdapter;
import com.example.truckavenue.ui.widgets.dialogs.AlertDialogs;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */
public class HistoryFragment extends MvpAppCompatFragment implements HistoryView, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.history_recycler_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    @BindView(R.id.empty_view)
    View mEmptyView;

    @BindView(R.id.progress_bar)
    View mProgressView;

    private MainActivityListener mActivityListener;

    @InjectPresenter
    HistoryPresenter mHistoryPresenter;

    private HistoryAdapter mAdapter;

    public static HistoryFragment newInstance() {
        HistoryFragment fragment = new HistoryFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivityListener = ((MainActivityListener) context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHistoryPresenter.getHistoryList();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_history, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);

        mAdapter = new HistoryAdapter(getActivity());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        mSwipeRefreshLayout.setOnRefreshListener(this);
    }

    @OnClick(R.id.toolbar_notifications)
    void onNotificationClicked() {
        mActivityListener.onNotificationClicked();
    }


    @Override
    public void showNoInternetConnection() {
        AlertDialogs.showNoConnectionMessage(getActivity(),
                (dialog, which) -> dialog.dismiss(),
                (dialog, which) -> dialog.dismiss());
    }

    @Override
    public void setHistoryList(List<HistoryResponse.History> historyList) {
        if (historyList.size() == 0) {
            mEmptyView.setVisibility(View.VISIBLE);
        } else {
            mEmptyView.setVisibility(View.GONE);
        }
        mAdapter.setHistoryList(historyList);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void errorInLoading() {
        AlertDialogs.showErrorDialog(getActivity());
    }

    @Override
    public void showProgress() {
        mSwipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onRefresh() {
        mHistoryPresenter.getHistoryList();
    }
}

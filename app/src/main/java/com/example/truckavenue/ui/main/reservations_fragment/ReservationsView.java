package com.example.truckavenue.ui.main.reservations_fragment;

import com.arellomobile.mvp.MvpView;
import com.example.truckavenue.data.network.entity.HistoryResponse;

import java.util.List;

public interface ReservationsView extends MvpView {
    void showNoInternetConnection();

    void loadSuccess();

    void setReservationsList(List<HistoryResponse.History> reservationsList);

    void errorInLoading();

    void showProgress();

    void hideProgress();
}

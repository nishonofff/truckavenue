package com.example.truckavenue.ui.main.reservations_fragment;

import android.annotation.SuppressLint;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.truckavenue.App;
import com.example.truckavenue.data.network.ApiService;
import com.example.truckavenue.data.network.entity.HistoryResponse;

import javax.inject.Inject;

@InjectViewState
public class ReservationsPresenter extends MvpPresenter<ReservationsView> {

    private static final String TAG = ReservationsPresenter.class.getName();

    @Inject
    ApiService mApiService;

    ReservationsPresenter() {
        App.getAppComponent().inject(this);
    }

    @SuppressLint("CheckResult")
    void getRservationsList() {
        if (mApiService.noConnection()) {
            getViewState().showNoInternetConnection();
            return;
        }
        getViewState().showProgress();

        mApiService.getReservationList().subscribe((HistoryResponse response) -> {
                    getViewState().hideProgress();
                    getViewState().setReservationsList(response.result.data);
                },
                (Throwable throwable) -> {
                    getViewState().hideProgress();
                });
    }

}

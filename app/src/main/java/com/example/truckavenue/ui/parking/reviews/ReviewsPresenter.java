package com.example.truckavenue.ui.parking.reviews;

import android.annotation.SuppressLint;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.truckavenue.App;
import com.example.truckavenue.data.network.ApiService;
import com.example.truckavenue.data.network.entity.ReviewResponse;

import javax.inject.Inject;

@InjectViewState
public class ReviewsPresenter extends MvpPresenter<ReviewsView> {

    private static final String TAG = ReviewsPresenter.class.getName();

    @Inject
    ApiService mApiService;

    ReviewsPresenter() {
        App.getAppComponent().inject(this);
    }

    @SuppressLint("CheckResult")
    void getAllReviews(int parkingId) {
        if (mApiService.noConnection()) {
            getViewState().showNoInternetConnection();
            return;
        }

        getViewState().showProgress();

        mApiService.getAllReviews(parkingId).subscribe((ReviewResponse response) -> {
                    getViewState().hideProgress();
                    getViewState().setReviewList(response.result.data);
                },
                (Throwable throwable) -> {
                    getViewState().hideProgress();
                    getViewState().loadParkingListError();
                });
    }


}

package com.example.truckavenue.ui.parking.add_comment;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RatingBar;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.truckavenue.R;
import com.example.truckavenue.ui.parking.parking_info.ParkingInfoActivity;
import com.example.truckavenue.ui.widgets.dialogs.AlertDialogs;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddCommentActivity extends MvpAppCompatActivity implements AddCommentView {

    @BindView(R.id.comment_edit_text)
    EditText mCommentEditText;

    @BindView(R.id.comment_rating_bar)
    RatingBar mCommentRatingBar;

    @BindView(R.id.progress_bar)
    View mProgressView;

    @InjectPresenter
    AddCommentPresenter mAddCommentPresenter;

    private int mParkingID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_comment);

        mParkingID = getIntent().getIntExtra(ParkingInfoActivity.KEY_PARKING_ID, 0);

        ButterKnife.bind(this);
    }

    @OnClick(R.id.toolbar_back_button)
    void onToolbarBackClicked() {
        onBackPressed();
    }

    @OnClick(R.id.send_comment_button)
    void onSendCommentClicked() {
        int rate = mCommentRatingBar.getNumStars();
        String comment = mCommentEditText.getText().toString();

        mAddCommentPresenter.addComment(mParkingID, comment, String.valueOf(rate));
    }

    @Override
    public void showNoInternetConnection() {
        AlertDialogs.showNoConnectionMessage(this,
                (dialog, which) -> dialog.dismiss(),
                (dialog, which) -> dialog.dismiss());
    }

    @Override
    public void addSuccess() {
        AlertDialogs.showSuccessDialog(this, "Your review successfully sent !");
    }

    @Override
    public void addError() {
        AlertDialogs.showErrorDialog(this);
    }

    @Override
    public void showProgress() {
        mProgressView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mProgressView.setVisibility(View.GONE);
    }
}

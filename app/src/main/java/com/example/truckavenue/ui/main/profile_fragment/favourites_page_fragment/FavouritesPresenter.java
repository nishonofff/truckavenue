package com.example.truckavenue.ui.main.profile_fragment.favourites_page_fragment;

import android.annotation.SuppressLint;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.truckavenue.App;
import com.example.truckavenue.data.network.ApiService;
import com.example.truckavenue.data.network.entity.FavouriteListResponse;

import javax.inject.Inject;
@InjectViewState
public class FavouritesPresenter extends MvpPresenter<FavouritesView> {

    private static final String TAG = FavouritesPresenter.class.getName();

    @Inject
    ApiService mApiService;

    FavouritesPresenter() {
        App.getAppComponent().inject(this);
    }

    @SuppressLint("CheckResult")
    void getFavouritesList() {
        if (mApiService.noConnection()) {
            getViewState().showNoInternetConnection();
            return;
        }
        getViewState().showProgress();

        mApiService.getFavouriteList().subscribe((FavouriteListResponse response) -> {
                    getViewState().hideProgress();
                    getViewState().setFavouriteList(response.result.favouriteList);
                },
                (Throwable throwable) -> {
                    getViewState().hideProgress();
                    getViewState().errorInLoading();
                });
    }
}

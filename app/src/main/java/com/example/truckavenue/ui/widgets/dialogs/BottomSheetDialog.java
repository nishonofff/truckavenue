package com.example.truckavenue.ui.widgets.dialogs;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.TextView;

import com.example.truckavenue.R;
import com.example.truckavenue.data.network.entity.ParkingResponse;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */

public class BottomSheetDialog extends android.support.design.widget.BottomSheetDialog {

    @BindView(R.id.parking_title)
    TextView mParkingTitle;
    @BindView(R.id.parking_address)
    TextView mParkingAddress;

    private BottomSheetDialog mDialog;

    public BottomSheetDialog(@NonNull Context context, ParkingResponse.Parking parkingItem) {
        super(context, R.style.BottomSheetDialogTheme);
        setContentView(R.layout.dialog_bottom_sheet);
        mDialog = this;
        ButterKnife.bind(this);

        mParkingTitle.setText(parkingItem.title);
        mParkingAddress.setText(parkingItem.address);
    }


    /**
     * Interface used to allow the creator of a dialog to run some code when an
     * item on the dialog is clicked.
     */
    public interface OnClickListener {

        /**
         * This method will be invoked when a button in the dialog is clicked.
         *
         * @param dialog the dialog that received the click
         */
        void onClick(BottomSheetDialog dialog);
    }

    public static class Builder {

        private BottomSheetDialog mBottomSheetDialog;

        public Builder(@NonNull Context context, ParkingResponse.Parking parkingItem) {
            mBottomSheetDialog = new BottomSheetDialog(context, parkingItem);
        }

//        public Builder setIcon(Drawable icon) {
//            mBottomSheetDialog.setIcon(icon);
//            return this;
//        }
//
//        public Builder setIcon(@DrawableRes int iconResId) {
//            mBottomSheetDialog.setIcon(iconResId);
//            return this;
//        }
//
//        public Builder setIconBackground(Drawable iconBackground) {
//            mBottomSheetDialog.setIconBackground(iconBackground);
//            return this;
//        }
//
//        public Builder setIconBackground(@DrawableRes int iconBackgroundResId) {
//            mBottomSheetDialog.setIconBackground(iconBackgroundResId);
//            return this;
//        }
//
//        public Builder setTitle(String title) {
//            mBottomSheetDialog.setTitle(title);
//            return this;
//        }
//
//        public Builder setTitle(@StringRes int titleResId) {
//            mBottomSheetDialog.setTitle(titleResId);
//            return this;
//        }
//
//        public Builder setMessage(String message) {
//            mBottomSheetDialog.setMessage(message);
//            return this;
//        }
//
//        public Builder setMessage(@StringRes int messageResId) {
//            mBottomSheetDialog.setMessage(messageResId);
//            return this;
//        }
//
//        public Builder setPositiveButton(String text, OnClickListener onClickListener) {
//            mBottomSheetDialog.setPositiveButton(text, onClickListener);
//            return this;
//        }
//
//        public Builder setPositiveButton(@StringRes int textResId, OnClickListener onClickListener) {
//            mBottomSheetDialog.setPositiveButton(textResId, onClickListener);
//            return this;
//        }
//
//        public Builder setNegativeButton(String text, OnClickListener onClickListener) {
//            mBottomSheetDialog.setNegativeButton(text, onClickListener);
//            return this;
//        }
//
//        public Builder setNegativeButton(@StringRes int textResId, OnClickListener onClickListener) {
//            mBottomSheetDialog.setNegativeButton(textResId, onClickListener);
//            return this;
//        }
//
//        public Builder setNeutralButton(String text, OnClickListener onClickListener) {
//            mBottomSheetDialog.setNeutralButton(text, onClickListener);
//            return this;
//        }
//
//        public Builder setNeutralButton(@StringRes int textResId, OnClickListener onClickListener) {
//            mBottomSheetDialog.setNeutralButton(textResId, onClickListener);
//            return this;
//        }

        public Builder setCancelable(boolean cancelable) {
            mBottomSheetDialog.setCancelable(cancelable);
            return this;
        }

        public BottomSheetDialog show() {
            mBottomSheetDialog.show();
            return mBottomSheetDialog;
        }
    }
}
package com.example.truckavenue.ui.add_payment;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.truckavenue.R;
import com.example.truckavenue.ui.widgets.dialogs.AlertDialogs;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddPaymentActivity extends MvpAppCompatActivity implements AddPaymentView {

    @InjectPresenter
    AddPaymentPresenter mAddPaymentPresenter;

    @BindView(R.id.cardholder_name_edit_text)
    TextInputEditText mCardHolderName;

    @BindView(R.id.card_number_edit_text)
    TextInputEditText mCardNumber;

    @BindView(R.id.card_expire_date_edit_text)
    TextInputEditText mCardExpireDate;

    @BindView(R.id.card_cvv_edit_text)
    TextInputEditText mCardCVV;

    @BindView(R.id.progress_bar)
    View mProgressView;

    private int mMonth;
    private int mYear;
    private AlertDialog mAlertDialog = null;
    private DatePickerDialog mDatePicker;
    private DatePickerDialog.OnDateSetListener mDateSetListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_payment);

        ButterKnife.bind(this);

        setUpDatePickerDialog();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @OnClick(R.id.toolbar_back_button)
    void onToolbarBackClicked() {
        onBackPressed();
    }

    @OnClick(R.id.add_payment_button)
    void onAddPaymentClicked() {
        String cardholderName = mCardHolderName.getText().toString();
        if (cardholderName.isEmpty()) {
            mCardHolderName.setError(getString(R.string.empty_msg_user_name));
            return;
        }
        String cardNumber = mCardNumber.getText().toString();
        if (cardNumber.isEmpty()) {
            mCardNumber.setError(getString(R.string.empty_msg_phone_number));
            return;
        }

        String expireDate = mCardExpireDate.getText().toString();
        if (expireDate.isEmpty()) {
            mCardExpireDate.setError(getString(R.string.empty_msg_user_name));
            return;
        }

        String cvv = mCardCVV.getText().toString();
        if (cvv.isEmpty()) {
            mCardCVV.setError(getString(R.string.empty_msg_user_name));
            return;
        }

        Toast.makeText(this, expireDate, Toast.LENGTH_SHORT).show();

        mAddPaymentPresenter.addPaymentMethod(cardholderName, cardNumber, mMonth, mYear, cvv);
    }

    @SuppressLint("DefaultLocale")
    @OnClick(R.id.card_expire_date_edit_text)
    void onExpireDateEdited() {
        mDateSetListener = (view, year, monthOfYear, dayOfMonth) -> {
            mCardExpireDate.setText(String.format("%d/%d", monthOfYear + 1, year));
            mMonth = monthOfYear;
            mYear = year;
        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            mDatePicker.setOnDateSetListener(mDateSetListener);
        }

        mDatePicker.show();
    }


    void setUpDatePickerDialog() {
        final Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        mDatePicker = new DatePickerDialog(this
                , android.R.style.Theme_Holo_Light_Dialog_MinWidth
                , mDateSetListener
                , year, month, day);
        mDatePicker.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    @Override
    public void showNoInternetConnection() {

    }

    @Override
    public void addSuccess() {
        AlertDialogs.showSuccessDialog(this,"Your card successfully added !");
    }

    @Override
    public void addError() {
        AlertDialogs.showErrorDialog(this);
    }

    @Override
    public void showProgress() {
        mProgressView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mProgressView.setVisibility(View.GONE);
    }
}

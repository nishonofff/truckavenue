package com.example.truckavenue.data.network;

import com.example.truckavenue.data.network.entity.BookingResponse;
import com.example.truckavenue.data.network.entity.CardListResponse;
import com.example.truckavenue.data.network.entity.CardResponse;
import com.example.truckavenue.data.network.entity.CommentResponse;
import com.example.truckavenue.data.network.entity.FavouriteListResponse;
import com.example.truckavenue.data.network.entity.HistoryResponse;
import com.example.truckavenue.data.network.entity.NotificationResponse;
import com.example.truckavenue.data.network.entity.ParkingResponse;
import com.example.truckavenue.data.network.entity.PaymentResponse;
import com.example.truckavenue.data.network.entity.ReviewResponse;
import com.example.truckavenue.data.network.entity.auth.ConfirmRequest;
import com.example.truckavenue.data.network.entity.auth.LoginRequest;
import com.example.truckavenue.data.network.entity.auth.response.ConfirmResponse;
import com.example.truckavenue.data.network.entity.auth.response.LoginResponse;
import com.example.truckavenue.data.network.entity.auth.response.RegisterResponse;
import com.example.truckavenue.data.network.entity.auth.response.UserResponse;
import com.example.truckavenue.data.network.entity.truck.TruckModelResponse;
import com.example.truckavenue.data.network.entity.truck.TruckResponse;

import java.util.Map;

import io.reactivex.Single;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */

public interface ApiService {
    ///if app language will be needed
    void setLocale(String locale);

    void setAuthKeys(String accessToken);

    /**
     * Checking internet connection
     *
     * @return true if there is not internet connection, otherwise false
     */
    boolean noConnection();

    Single<RegisterResponse> register(Map<String, String> body);

    Single<LoginResponse> login(LoginRequest body);

    Single<ConfirmResponse> confirm(Map<String, String> body);

    Single<TruckResponse> getTruckList();

    Single<TruckModelResponse> getTruckModelList();

    Single<CardListResponse> getCardList();

    Single<ParkingResponse> getParkingList();

    Single<NotificationResponse> getNotificationList();

    Single<CommentResponse> addComment(Map<String, Object> body);

    Single<FavouriteListResponse> getFavouriteList();

    Single<HistoryResponse> getHistoryList();

    Single<HistoryResponse> getReservationList();

    Single<TruckResponse> addTruck(RequestBody truckNumber,
                                   RequestBody truckModel,
                                   MultipartBody.Part truckImage);

    Single<TruckResponse> addTruck(RequestBody truckNumber,
                                   RequestBody truckModel,
                                   MultipartBody.Part truckImage,
                                   Boolean isActive);

    Single<TruckResponse> updateTruck(int truckId,
                                      RequestBody truckNumber,
                                      RequestBody truckModel,
                                      Boolean isActive);

    Single<TruckResponse> updateTruck(int truckId,
                                      RequestBody truckNumber,
                                      RequestBody truckModel,
                                      MultipartBody.Part truckImage,
                                      Boolean isActive);


    Single<TruckResponse> deleteTruck(int truckId);

    Single<ConfirmResponse> getFavouriteItem(ConfirmRequest body);

    Single<UserResponse> getUserInfo();

    Single<UserResponse> updateUserInfo(RequestBody username,
                                        RequestBody phoneNumber,
                                        RequestBody email,
                                        MultipartBody.Part avatar);

    Single<UserResponse> updateUserInfo(RequestBody username,
                                        RequestBody phoneNumber,
                                        RequestBody email);

    Single<ConfirmResponse> addToFavouriteList(ConfirmRequest body);

    Single<CardResponse> addCard(Map<String, String> body);

    Single<CardResponse> deleteCard(int cardId);

    Single<BookingResponse> addBooking(Map<String, Object> body);

    Single<PaymentResponse> makePayment(Map<String, Integer> body);

    Single<ReviewResponse> getAllReviews(int parkingId);

}

package com.example.truckavenue.ui.parking.find_parking;

import android.annotation.SuppressLint;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.truckavenue.App;
import com.example.truckavenue.data.network.ApiService;
import com.example.truckavenue.data.network.entity.ParkingResponse;

import javax.inject.Inject;

@InjectViewState
public class FindParkingPresenter extends MvpPresenter<FindParkingView> {

    private static final String TAG = FindParkingPresenter.class.getName();

    @Inject
    ApiService mApiService;

    FindParkingPresenter() {
        App.getAppComponent().inject(this);
    }

    @SuppressLint("CheckResult")
    void getParkingList() {
        if (mApiService.noConnection()) {
            getViewState().showNoInternetConnection();
            return;
        }

        getViewState().showProgress();

        mApiService.getParkingList().subscribe((ParkingResponse response) -> {
                    getViewState().hideProgress();
                    getViewState().setParkingList(response.result.data);
                },
                (Throwable throwable) -> {
                    getViewState().hideProgress();
                    getViewState().loadParkingListError();
                });
    }


}

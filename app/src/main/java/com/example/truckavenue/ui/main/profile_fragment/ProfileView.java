package com.example.truckavenue.ui.main.profile_fragment;

import com.arellomobile.mvp.MvpView;
import com.example.truckavenue.data.network.entity.auth.response.UserResponse;

public interface ProfileView extends MvpView {
    void showNoInternetConnection();

    void setUserInfo(UserResponse.Result userInfo);

    void errorInLoading();

    void showProgress();

    void hideProgress();
}

package com.example.truckavenue.data.network.entity;

import com.squareup.moshi.Json;

import java.io.Serializable;
import java.util.List;

public class ReviewResponse extends BaseResponse implements Serializable{

    @Json(name = "result")
    public Result result;

    public static class Result implements Serializable {
        @Json(name = "comments")
        public List<Review> data = null;
    }

    public static class Review implements Serializable {
        public int id;
        @Json(name = "parking")
        public int parking;
        @Json(name = "user")
        public User user;
        @Json(name = "comment")
        public String comment;
        @Json(name = "rate")
        public int rate;
        @Json(name = "created")
        public String created;
    }

    public static class User implements Serializable {
        int id;
        @Json(name = "avatar")
        public String avatar;
        @Json(name = "username")
        public String username;
        @Json(name = "phone_number")
        public String phoneNumber;
        @Json(name = "email")
        public String email;
    }

}

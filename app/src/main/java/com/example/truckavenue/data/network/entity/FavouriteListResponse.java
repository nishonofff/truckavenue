package com.example.truckavenue.data.network.entity;

import com.squareup.moshi.Json;

import java.util.List;

public class FavouriteListResponse extends BaseResponse {

    @Json(name = "result")
    public Result result;

    public static class Result {
        @Json(name = "favorites")
        public List<Favourite> favouriteList = null;
    }

    public static class Favourite {
        @Json(name = "id")
        public int id ;
        @Json(name = "parking")
        public ParkingResponse.Parking parking;
    }

}

package com.example.truckavenue.ui.update_user_details;

import com.arellomobile.mvp.MvpView;
import com.example.truckavenue.data.network.entity.auth.response.UserResponse;

public interface UpdateUserView extends MvpView {

    void showNoInternetConnection();

    void setUserInfo(UserResponse.Result userInfo);

    void updateUserSuccess();

    void updateUserError();

    void showProgress();

    void hideProgress();

}

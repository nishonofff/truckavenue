package com.example.truckavenue.ui.parking.find_parking;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.truckavenue.R;
import com.example.truckavenue.data.network.entity.ParkingResponse;
import com.example.truckavenue.ui.parking.parking_info.ParkingInfoActivity;
import com.example.truckavenue.ui.widgets.adapters.ParkingsAdapter;
import com.example.truckavenue.ui.widgets.dialogs.AlertDialogs;
import com.example.truckavenue.utils.AppConstants;

import java.io.Serializable;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FindParkingActivity extends MvpAppCompatActivity implements ParkingsAdapter.ParkingItemClickListener,
        FindParkingView, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    @BindView(R.id.parking_recycler_view)
    RecyclerView mRecyclerView;

    @InjectPresenter
    FindParkingPresenter mFindParkingPresenter;

    private ParkingsAdapter mParkingAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_parking);

        ButterKnife.bind(this);

        mParkingAdapter = new ParkingsAdapter(this);
        mParkingAdapter.setItemClickListener(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mFindParkingPresenter.getParkingList();

        mSwipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @OnClick(R.id.toolbar_back_button)
    void onToolbarBackClicked() {
        onBackPressed();
    }

    @Override
    public void onParkingItemClick(int position, ParkingResponse.Parking parkingItem) {
        Intent intent = new Intent(this, ParkingInfoActivity.class);
        intent.putExtra(AppConstants.KEY_PARKING_ITEM, (Serializable) parkingItem);
        startActivity(intent);
    }

    @Override
    public void showNoInternetConnection() {
        AlertDialogs.showNoConnectionMessage(this,
                (dialog, which) -> dialog.dismiss(),
                (dialog, which) -> dialog.dismiss());
    }

    @Override
    public void setParkingList(List<ParkingResponse.Parking> parkingList) {
        mParkingAdapter.setParkingList(parkingList);
        mRecyclerView.setAdapter(mParkingAdapter);
    }

    @Override
    public void loadParkingListError() {
        AlertDialogs.showErrorDialog(this);
    }

    @Override
    public void showProgress() {
        mSwipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onRefresh() {
        mFindParkingPresenter.getParkingList();
    }
}

package com.example.truckavenue.ui.auth.login_fragment;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.truckavenue.R;
import com.example.truckavenue.ui.auth.callback.AuthActivityListener;
import com.example.truckavenue.ui.auth.callback.LoginFragmentCallback;
import com.example.truckavenue.utils.AppUtils;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, May 24
 */

public class LoginFragment extends Fragment implements LoginFragmentCallback {

    @BindView(R.id.user_email)
    TextInputEditText mEmail;
    @BindView(R.id.signup_phone_number)
    TextInputEditText mSignUpPhoneNumber;
    @BindView(R.id.user_name)
    TextInputEditText mUserName;
    @BindView(R.id.login_phone_number)
    TextInputEditText mLoginPhoneNumber;
    @BindView(R.id.sign_up)
    TextView mSignUp;
    @BindView(R.id.log_in)
    TextView mLogIn;
    @BindView(R.id.sign_up_view)
    View mSignUpView;
    @BindView(R.id.log_in_view)
    View mLogInView;
    @BindView(R.id.progress_bar)
    View mProgressView;
    @BindView(R.id.container_view)
    View mContainerView;
    @BindView(R.id.log_in_button)
    Button mLogInButton;
    @BindColor(R.color.colorPrimaryText)
    int mPrimaryText;
    @BindColor(R.color.colorSecondaryText)
    int mSecondaryText;

    public static final String TAG = LoginFragment.class.getName();
    private AuthActivityListener mActivityListener;

    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivityListener = (AuthActivityListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        mLoginPhoneNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() < 12) {
                    mLogInButton.setBackground(getActivity().getDrawable(R.drawable.gradient_secondary));
                    mLogInButton.setEnabled(false);
                } else {
                    mLogInButton.setBackground(getActivity().getDrawable(R.drawable.gradient_primary));
                    mLogInButton.setEnabled(true);
                }
            }
        });
    }

    @OnClick(R.id.log_in)
    void onLogin() {
        mLogIn.setTextColor(mPrimaryText);
        mSignUp.setTextColor(mSecondaryText);
        mSignUpView.setVisibility(View.GONE);
        mLogInView.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.sign_up)
    void onSignUp() {
        mLogIn.setTextColor(mSecondaryText);
        mSignUp.setTextColor(mPrimaryText);
        mSignUpView.setVisibility(View.VISIBLE);
        mLogInView.setVisibility(View.GONE);
    }

    @OnClick(R.id.log_in_button)
    void onLoginClicked() {
        String phoneNumber = mLoginPhoneNumber.getText().toString();
        if (phoneNumber.isEmpty()) {
            mLoginPhoneNumber.setError(getString(R.string.empty_msg_phone_number));
            return;
        }
        mActivityListener.onLogin(this, phoneNumber);
    }

    @OnClick(R.id.sign_up_button)
    void onSignUpClicked() {
        String email = mEmail.getText().toString();
        if (email.isEmpty()) {
            mEmail.setError(getString(R.string.empty_msg_user_name));
            return;
        }

        String phoneNumber = mSignUpPhoneNumber.getText().toString();
        if (phoneNumber.isEmpty()) {
            mSignUpPhoneNumber.setError(getString(R.string.empty_msg_phone_number));
            return;
        }

        String userName = mUserName.getText().toString();
        if (userName.isEmpty()) {
            mUserName.setError(getString(R.string.empty_msg_user_name));
            return;
        }
        mActivityListener.onSignUp(this, phoneNumber, userName, email);
    }

    @OnFocusChange(R.id.login_phone_number)
    void onLoginPhoneNumberFocusChanged(boolean focused) {
        if (mLoginPhoneNumber.getText().length() < 3) {
            if (focused)
                mLoginPhoneNumber.setText("+1");
            else {
                mLoginPhoneNumber.setText("");
                AppUtils.hideKeyboard(getActivity(), mLoginPhoneNumber);
            }
        }
    }

    @OnFocusChange(R.id.signup_phone_number)
    void onSigUpPhoneNumberFocusChanged(boolean focused) {
        if (mSignUpPhoneNumber.getText().length() < 3) {
            if (focused)
                mSignUpPhoneNumber.setText("+1");
            else {
                mSignUpPhoneNumber.setText("");
                AppUtils.hideKeyboard(getActivity(), mSignUpPhoneNumber);
            }
        }
    }

    @OnClick(R.id.container_view)
    void onContainerClicked() {
        mContainerView.requestFocus();
    }

    @Override
    public void showLoginProgress() {
        mProgressView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoginProgress() {
        mProgressView.setVisibility(View.GONE);
    }

    @Override
    public void onErrorEmail(String message) {
        mEmail.setError(getString(R.string.empty_msg_email));
    }

    @Override
    public void onErrorPhoneNumber(String message) {
        mSignUpPhoneNumber.setError(getString(R.string.empty_msg_phone_number));
    }

    @Override
    public void onErrorUserName(String message) {
        mUserName.setError(getString(R.string.empty_msg_user_name));
    }
}

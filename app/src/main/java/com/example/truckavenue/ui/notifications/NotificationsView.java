package com.example.truckavenue.ui.notifications;

import com.arellomobile.mvp.MvpView;
import com.example.truckavenue.data.network.entity.NotificationResponse;

import java.util.List;

public interface NotificationsView extends MvpView {
    void showNoInternetConnection();

    void setNotifications(List<NotificationResponse.Notification> notificationsList);

    void getNotificationsError();

    void showProgress();

    void hideProgress();
}

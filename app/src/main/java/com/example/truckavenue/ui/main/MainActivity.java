package com.example.truckavenue.ui.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.example.truckavenue.R;
import com.example.truckavenue.ui.main.callback.MainActivityListener;
import com.example.truckavenue.ui.main.history_fragment.HistoryFragment;
import com.example.truckavenue.ui.main.profile_fragment.ProfileFragment;
import com.example.truckavenue.ui.main.reservations_fragment.ReservationsFragment;
import com.example.truckavenue.ui.main.search_fragment.SearchFragment;
import com.example.truckavenue.ui.notifications.NotificationsActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */
public class MainActivity extends AppCompatActivity implements MainActivityListener {


    @BindView(R.id.bottom_navbar_search)
    ImageView mNavbarSearch;

    @BindView(R.id.bottom_navbar_history)
    ImageView mNavbarHistory;

    @BindView(R.id.bottom_navbar_profile)
    ImageView mNavbarProfile;

    @BindView(R.id.bottom_navbar_reservations)
    ImageView mNavbarReservations;

    private FragmentManager mFragmentManager;
    private MainActivity mContext;

    public static void start(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mContext = this;
        mFragmentManager = getSupportFragmentManager();
        onSearchClick();
    }

    @OnClick(R.id.bottom_navbar_search)
    public void onSearchClick() {
        clearSelectionBottomIcons();
        mNavbarSearch.setSelected(true);
        mFragmentManager.beginTransaction()
                .replace(R.id.container, SearchFragment.newInstance())
//                .addToBackStack(NewsFragment.TAG)
                .commit();
    }

    @OnClick(R.id.bottom_navbar_reservations)
    public void onReservationsClick() {
        clearSelectionBottomIcons();
        mNavbarReservations.setSelected(true);
        mFragmentManager.beginTransaction()
                .replace(R.id.container, ReservationsFragment.newInstance())
//                .addToBackStack(NewsFragment.TAG)
                .commit();
    }

    @OnClick(R.id.bottom_navbar_history)
    public void onHistoryClick() {
        clearSelectionBottomIcons();
        mNavbarHistory.setSelected(true);
        mFragmentManager.beginTransaction()
                .replace(R.id.container, HistoryFragment.newInstance())
//                .addToBackStack(NewsFragment.TAG)
                .commit();
    }

    @OnClick(R.id.bottom_navbar_profile)
    public void onProfileClick() {
        clearSelectionBottomIcons();
        mNavbarProfile.setSelected(true);
        mFragmentManager.beginTransaction()
                .replace(R.id.container, ProfileFragment.newInstance())
//                .addToBackStack(NewsFragment.TAG)
                .commit();
    }

    public void clearSelectionBottomIcons() {
        mNavbarSearch.setSelected(false);
        mNavbarReservations.setSelected(false);
        mNavbarHistory.setSelected(false);
        mNavbarProfile.setSelected(false);
    }

    @Override
    public void onNotificationClicked() {
        Intent intent = new Intent(this, NotificationsActivity.class);
        startActivity(intent);
    }
}


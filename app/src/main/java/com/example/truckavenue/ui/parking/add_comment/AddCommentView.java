package com.example.truckavenue.ui.parking.add_comment;

import com.arellomobile.mvp.MvpView;

public interface AddCommentView extends MvpView {
    void showNoInternetConnection();

    void addSuccess();

    void addError();

    void showProgress();

    void hideProgress();
}

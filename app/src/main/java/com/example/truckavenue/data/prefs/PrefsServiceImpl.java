package com.example.truckavenue.data.prefs;

import android.content.Context;

import com.example.truckavenue.utils.AppConstants;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */

public class PrefsServiceImpl implements PrefsService, AppConstants {

    private final SecuredPreferencesHelper mSecuredPrefs;

    public PrefsServiceImpl(Context context, String prefsFileName) {
        mSecuredPrefs = new SecuredPreferencesHelper(context, prefsFileName);
    }

    @Override
    public void clearPrefs() {
        mSecuredPrefs.clearAllData();
    }

    @Override
    public String getLocale() {
        return mSecuredPrefs.get(LOCALE, "ru");
    }

    @Override
    public void setLocale(String locale) {
        mSecuredPrefs.put(LOCALE, locale);
    }

    @Override
    public String getToken(String key) {
        return mSecuredPrefs.get(key, "0");
    }

    @Override
    public void setToken(String key, String token) {
        mSecuredPrefs.put(key, token);
    }

    @Override
    public void clearUserData() {
        mSecuredPrefs.deleteSavedData("token");
    }
}

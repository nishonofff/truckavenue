package com.example.truckavenue.ui.parking.find_parking;

import com.arellomobile.mvp.MvpView;
import com.example.truckavenue.data.network.entity.ParkingResponse;

import java.util.List;

public interface FindParkingView extends MvpView {

    void showNoInternetConnection();

    void setParkingList(List<ParkingResponse.Parking> parkingList);

    void loadParkingListError();

    void showProgress();

    void hideProgress();

}

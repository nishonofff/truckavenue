package com.example.truckavenue.ui.booking;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.truckavenue.R;
import com.example.truckavenue.data.network.entity.BookingResponse;
import com.example.truckavenue.data.network.entity.CardListResponse;
import com.example.truckavenue.data.network.entity.ParkingResponse;
import com.example.truckavenue.ui.widgets.adapters.CardsAdapter;
import com.example.truckavenue.ui.widgets.adapters.PaymentsAdapter;
import com.example.truckavenue.ui.widgets.dialogs.AlertDialogs;
import com.example.truckavenue.utils.AppUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BookingActivity extends MvpAppCompatActivity implements BookingView,
        PaymentsAdapter.PaymentItemClickListener, CardsAdapter.CardItemClickListener {

    @BindView(R.id.truck_item)
    View mTruckItemView;
    @BindView(R.id.start_date_edit_text)
    EditText mStartDateView;
    @BindView(R.id.end_date_edit_text)
    EditText mEndDateView;
    @BindView(R.id.booking_dates)
    TextView mBookingDate;
    @BindView(R.id.booking_days)
    TextView mBookingDays;
    @BindView(R.id.booking_price)
    TextView mBookingPrice;
    @BindView(R.id.booking_price_view)
    View mBookingPriceView;
    @BindView(R.id.cards_recycler_view)
    RecyclerView mCardsRecyclerView;
    @BindView(R.id.progress_bar)
    View mProgressView;

    @InjectPresenter
    BookingPresenter mBookingPresenter;

    private String mStartDate = null, mEndDate = null;
    private ParkingResponse.Parking mParkingItem;
    private CardListResponse.Card mCardItem;

    private AlertDialog mAlertDialog = null;
    private DatePickerDialog mDatePicker;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    private CardsAdapter mAdapter;
    private List<CardListResponse.Card> mCardList = new ArrayList<>();
    private List<CardListResponse.Card> mAllCardsList = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking);

        ButterKnife.bind(this);

        setUpDatePickerDialog();

        mParkingItem = (ParkingResponse.Parking) getIntent().getSerializableExtra("PARKING_ITEM");

        mBookingPresenter.getPaymentsList();
        setUpCardsAdapter();
    }

    @OnClick(R.id.toolbar_back_button)
    void onToolbarBackClicked() {
        onBackPressed();
    }

//    @OnClick(R.id.select_truck)
//    void onSelectTruckClicked() {
//        RecyclerView recyclerView = new RecyclerView(this);
//        mAlertDialog = new AlertDialog.Builder(this)
//                .setTitle("Select truck")
//                .setView(recyclerView).create();
//
//        recyclerView.setLayoutManager(new LinearLayoutManager(this));
//        TrucksAdapter adapter = new TrucksAdapter(this);
//        recyclerView.setAdapter(adapter);
//        mAlertDialog.show();
//    }


    @SuppressLint("SetTextI18n")
    @OnClick(R.id.start_date_edit_text)
    void onStartDateEdited() {
        mDateSetListener = (view, year, month, day) -> {
            mStartDateView.setText(day + "/" + (month + 1) + "/" + year);
            mStartDate = year + "-" + (month + 1) + "-" + day;
        };
        mDatePicker.show();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            mDatePicker.setOnDateSetListener(mDateSetListener);
        }
    }

    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    @OnClick(R.id.end_date_edit_text)
    void onEndDateEdited() {
        mDateSetListener = (view, year, month, day) -> {
            mEndDateView.setText(day + "/" + (month + 1) + "/" + year);

            mEndDate = year + "-" + (month + 1) + "-" + day;

            long totalDays = AppUtils.getDifferenceOfDays(mStartDate, mEndDate);

            mBookingDate.setText(mStartDateView.getText().toString() + " - " + mEndDateView.getText().toString());
            mBookingDays.setText(String.format("%d days", totalDays));
            mBookingPrice.setText(String.format("%d $", totalDays * mParkingItem.prices.get(0).price));
            mBookingPriceView.setVisibility(View.VISIBLE);
        };

        mDatePicker.show();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            mDatePicker.setOnDateSetListener(mDateSetListener);
        }
    }

    @OnClick(R.id.add_payment_method_button)
    void onAddPaymentMethodClicked() {
        if (mAlertDialog != null)
            mAlertDialog.show();
    }

    private void setUpPaymentAdapter(List<CardListResponse.Card> cardList) {
        RecyclerView recyclerView = new RecyclerView(this);
        mAlertDialog = new AlertDialog.Builder(this)
                .setTitle("Select card")
                .setView(recyclerView).create();

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        PaymentsAdapter adapter = new PaymentsAdapter(this);
        adapter.setItemClickListener(this);
        adapter.setCardList(cardList);
        recyclerView.setAdapter(adapter);
    }

    void setUpDatePickerDialog() {
        final Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        mDatePicker = new DatePickerDialog(this
                , android.R.style.Theme_Holo_Light_Dialog_MinWidth
                , mDateSetListener
                , year, month, day);
        mDatePicker.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    @Override
    public void showNoInternetConnection() {

    }

    @Override
    public void setPaymentList(List<CardListResponse.Card> cardList) {
        setUpPaymentAdapter(cardList);
        mAllCardsList = cardList;
    }

    @Override
    public void showProgress() {
        mProgressView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mProgressView.setVisibility(View.GONE);
    }

    @Override
    public void bookingSuccess(BookingResponse.Result bookingResponse) {
        mBookingPresenter.makePayment(bookingResponse.id, mCardItem.id);
    }

    @Override
    public void paymentSuccess() {
        AlertDialogs.showSuccessDialog(this, "You have successfully ordered this parking");
    }

    @Override
    public void bookingError(String message) {
        AlertDialogs.showErrorDialog(this);
    }

    @Override
    public void notFoundActiveTruckError(String message) {
        AlertDialogs.showErrorDialog(this, message);
    }

    @Override
    public void errorInLoading() {

    }

    @Override
    public void onPaymentItemClick(int position) {
        if (!mCardList.contains(mAllCardsList.get(position))) {
            mCardList.add(mAllCardsList.get(position));
            mAdapter.notifyItemInserted(mCardList.size());
        }
        mAlertDialog.dismiss();
    }

    private void setUpCardsAdapter() {
        mCardsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new CardsAdapter(this);
        mAdapter.setItemClickListener(this);
        mAdapter.setCardList(mCardList);
        mCardsRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onCardItemClick(int position, CardListResponse.Card cardItem) {
        mCardItem = cardItem;
        mBookingPresenter.addBooking(mParkingItem.id, mStartDate, mEndDate);
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}

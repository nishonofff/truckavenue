package com.example.truckavenue.ui.main.profile_fragment.payment_page_fragment;

import com.arellomobile.mvp.MvpView;
import com.example.truckavenue.data.network.entity.CardListResponse;

import java.util.List;

public interface PaymentView extends MvpView {
    void showNoInternetConnection();

    void loadSuccess();

    void setPaymentList(List<CardListResponse.Card> cardList);

    void errorInLoading();

    void showProgress();

    void hideProgress();
}

package com.example.truckavenue.ui.auth.confirm_fragment;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

import com.example.truckavenue.R;
import com.example.truckavenue.ui.auth.callback.AuthActivityListener;
import com.example.truckavenue.ui.auth.callback.ConfirmFragmentCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, May 24
 */

public class ConfirmFragment extends Fragment implements ConfirmFragmentCallback {

    @BindView(R.id.confirm_code)
    TextInputEditText mConfirmCode;
    @BindView(R.id.confirm_button)
    Button mConfirmButton;
    @BindView(R.id.resend_image_view)
    ImageView mResendImageView;

    public static final String TAG = ConfirmFragment.class.getName();


    private AuthActivityListener mActivityListener;
    private Animation mAnimation;

    public static ConfirmFragment newInstance() {
        ConfirmFragment fragment = new ConfirmFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivityListener = (AuthActivityListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_confirm, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        mAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_rotation);

        mConfirmCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() < 5) {
                    mConfirmButton.setBackground(getActivity().getDrawable(R.drawable.gradient_secondary));
                    mConfirmButton.setEnabled(false);
                } else {
                    mConfirmButton.setBackground(getActivity().getDrawable(R.drawable.gradient_primary));
                    mConfirmButton.setEnabled(true);
                }
            }
        });
    }

    @OnClick(R.id.confirm_button)
    void onConfirmClicked() {
        String confirmCode = mConfirmCode.getText().toString();
        if (confirmCode.isEmpty()) {
            mConfirmCode.setError(getString(R.string.empty_msg_phone_number));
            return;
        }
        mActivityListener.onConfirmation(this, confirmCode);
    }

    @OnClick(R.id.toolbar_back_button)
    void onBackButtonClicked() {
        getActivity().onBackPressed();
    }

    @OnClick(R.id.resend_code_button)
    void onResendClicked() {
        mActivityListener.onResendConfirmCode();
    }

    @Override
    public void showConfirmProgress() {

    }

    @Override
    public void hideConfirmProgress() {

    }

    @Override
    public void onErrorConfirmCode(String message) {

    }


    private void animate(){
        mResendImageView.startAnimation(mAnimation);
    }
}

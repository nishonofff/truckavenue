package com.example.truckavenue.ui.settings;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.truckavenue.App;
import com.example.truckavenue.data.network.ApiService;
import com.example.truckavenue.data.prefs.PrefsService;

import javax.inject.Inject;

@InjectViewState
public class SettingsPresenter extends MvpPresenter<SettingsView> {

    private static final String TAG = SettingsPresenter.class.getName();

    @Inject
    ApiService mApiService;

    @Inject
    PrefsService mPrefsService;

    SettingsPresenter() {
        App.getAppComponent().inject(this);
    }

    public void onLogOut() {
        mPrefsService.clearUserData();
    }

}

package com.example.truckavenue.ui.main.profile_fragment.payment_page_fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.truckavenue.R;
import com.example.truckavenue.data.network.entity.CardListResponse;
import com.example.truckavenue.ui.add_payment.AddPaymentActivity;
import com.example.truckavenue.ui.widgets.adapters.PaymentsAdapter;
import com.example.truckavenue.ui.widgets.dialogs.AlertDialogs;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */
public class PaymentPageFragment extends MvpAppCompatFragment implements PaymentView, SwipeRefreshLayout.OnRefreshListener {


    @BindView(R.id.payments_recycler_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.empty_view)
    View mEmptyView;

    @BindView(R.id.progress_bar)
    View mProgressView;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    @InjectPresenter
    PaymentPresenter mPaymentPresenter;

    private PaymentsAdapter mAdapter;

    public static PaymentPageFragment newInstance() {
        PaymentPageFragment fragment = new PaymentPageFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        mPaymentPresenter.getPaymentsList();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_page_payment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);

        mSwipeRefreshLayout.setOnRefreshListener(this);

        mAdapter = new PaymentsAdapter(getActivity());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    @OnClick(R.id.add_payment_button)
    void onAddPaymentClicked() {
        Intent intent = new Intent(getActivity(), AddPaymentActivity.class);
        startActivity(intent);
    }

    @Override
    public void showNoInternetConnection() {
        AlertDialogs.showNoConnectionMessage(getActivity(),
                (dialog, which) -> dialog.dismiss(),
                (dialog, which) -> dialog.dismiss());
    }

    @Override
    public void loadSuccess() {

    }

    @Override
    public void setPaymentList(List<CardListResponse.Card> cardList) {
        if (cardList.size() == 0) {
            mEmptyView.setVisibility(View.VISIBLE);
        } else {
            mEmptyView.setVisibility(View.GONE);
        }
        mAdapter.setCardList(cardList);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void errorInLoading() {
        AlertDialogs.showErrorDialog(getActivity());
    }

    @Override
    public void showProgress() {
        mSwipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onRefresh() {
        mPaymentPresenter.getPaymentsList();
    }
}

package com.example.truckavenue.ui.widgets.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import com.example.truckavenue.R;


public class AlertDialogs {

    public static void showNoConnectionMessage(Context context, DialogInterface.OnClickListener positiveListener,
                                               DialogInterface.OnClickListener negativeListener) {
        new AlertDialog.Builder(context, R.style.AlertDialog)
                .setCancelable(false)
                .setMessage(R.string.check_internet_connection)
                .setPositiveButton(R.string.retry, positiveListener)
                .setNegativeButton(android.R.string.cancel, negativeListener)
                .show();
    }

    public static void logOutMessage(Context context, DialogInterface.OnClickListener positiveListener,
                                     DialogInterface.OnClickListener negativeListener) {
        new AlertDialog.Builder(context, R.style.AlertDialog)
                .setCancelable(false)
                .setMessage("Are you sure you wish to log out?")
                .setPositiveButton("Log out", positiveListener)
                .setNegativeButton(android.R.string.cancel, negativeListener)
                .show();
    }


    public static void showSuccessDialog(Context context) {
        new AlertDialog.Builder(context, R.style.AlertDialog)
                .setCancelable(true)
                .setTitle("Success !")
                .setMessage("Your order has been sent successfully !")
                .show();
    }

    public static void showSuccessDialog(Context context, String message) {
        new AlertDialog.Builder(context, R.style.AlertDialog)
                .setCancelable(true)
                .setTitle("Success !")
                .setMessage(message)
                .show();
    }

    public static void showErrorDialog(Context context) {
        new AlertDialog.Builder(context, R.style.AlertDialog)
                .setCancelable(true)
                .setTitle("Error !")
                .setMessage(R.string.check_internet_connection)
                .show();
    }

    public static void showErrorDialog(Context context, String message) {
        new AlertDialog.Builder(context, R.style.AlertDialog)
                .setCancelable(true)
                .setTitle("Error !")
                .setMessage(message)
                .show();
    }

}

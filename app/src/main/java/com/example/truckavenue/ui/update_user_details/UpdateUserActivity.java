package com.example.truckavenue.ui.update_user_details;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.truckavenue.R;
import com.example.truckavenue.data.network.entity.auth.response.UserResponse;
import com.example.truckavenue.ui.widgets.dialogs.AlertDialogs;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.truckavenue.utils.AppConstants.BASE_URL_PATH;

public class UpdateUserActivity extends MvpAppCompatActivity implements UpdateUserView {

    @BindView(R.id.profile_image_view)
    RoundedImageView mUserImage;

    @BindView(R.id.user_email)
    TextView mUserEmail;

    @BindView(R.id.user_name)
    TextView mUserName;

    @BindView(R.id.user_phone_number)
    TextView mUserPhoneNumber;

    @BindView(R.id.progress_bar)
    View mProgressView;

    @InjectPresenter
    UpdateUserPresenter mUpdateUserPresenter;

    private final int GALLERY_REQUEST_CODE = 1;

    private Uri mImageUri = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_user);

        ButterKnife.bind(this);

        mUpdateUserPresenter.getUserInfo();
    }

    @OnClick(R.id.profile_image_view)
    void onPickImageClicked() {
        pickFromGallery();
    }

    private void pickFromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, GALLERY_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == Activity.RESULT_OK)
            switch (requestCode) {
                case GALLERY_REQUEST_CODE:
                    Uri selectedImage = data.getData();
                    mImageUri = selectedImage;
                    mUserImage.setImageURI(selectedImage);
                    break;
            }
    }

    @Override
    public void setUserInfo(UserResponse.Result userInfo) {

        mUserEmail.setText(userInfo.email);
        mUserName.setText(userInfo.userName);
        mUserPhoneNumber.setText(userInfo.phoneNumber);

        Picasso.with(this)
                .load(BASE_URL_PATH + userInfo.avatar)
                .resize(mUserImage.getMeasuredWidth(), mUserImage.getMeasuredHeight())
                .centerCrop()
                .placeholder(R.drawable.profile_image_placeholder)
                .into(mUserImage);
    }

    @OnClick(R.id.toolbar_back_button)
    void onToolbarBackClicked() {
        onBackPressed();
    }

    @OnClick(R.id.save_button)
    void onSaveClicked() {
        String email = mUserEmail.getText().toString();
        String userName = mUserName.getText().toString();
        String phoneNumber = mUserPhoneNumber.getText().toString();

        mUpdateUserPresenter.updateUserInfo(this, userName, phoneNumber, email, mImageUri);
    }


    @Override
    public void showNoInternetConnection() {
        AlertDialogs.showNoConnectionMessage(this,
                (dialog, which) -> dialog.dismiss(),
                (dialog, which) -> dialog.dismiss());
    }

    @Override
    public void updateUserSuccess() {
        AlertDialogs.showSuccessDialog(this,"Your profile successfully updated !");
    }

    @Override
    public void updateUserError() {
        AlertDialogs.showErrorDialog(this);
    }

    @Override
    public void showProgress() {
        mProgressView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mProgressView.setVisibility(View.GONE);
    }
}

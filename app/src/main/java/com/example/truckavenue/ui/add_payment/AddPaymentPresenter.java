package com.example.truckavenue.ui.add_payment;

import android.annotation.SuppressLint;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.truckavenue.App;
import com.example.truckavenue.data.network.ApiService;
import com.example.truckavenue.data.network.entity.CardResponse;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

@InjectViewState
public class AddPaymentPresenter extends MvpPresenter<AddPaymentView> {
    private static final String TAG = AddPaymentPresenter.class.getName();

    @Inject
    ApiService mApiService;

    AddPaymentPresenter() {
        App.getAppComponent().inject(this);
    }

    @SuppressLint("CheckResult")
    void addPaymentMethod(String cardholderName, String cardNumber, int month, int year, String cvv) {
        if (mApiService.noConnection()) {
            getViewState().showNoInternetConnection();
            return;
        }
        getViewState().showProgress();

        Map<String, String> body = new HashMap<>();
        body.put("cardholder_name", cardholderName);
        body.put("number", cardNumber);
        body.put("exp_month", String.valueOf(month));
        body.put("exp_year", String.valueOf(year));
        body.put("cvv", cvv);

        mApiService.addCard(body).subscribe((CardResponse response) -> {
            getViewState().hideProgress();
            getViewState().addSuccess();
        }, (Throwable throwable) -> {
            getViewState().hideProgress();
            getViewState().addError();
        });
    }
}

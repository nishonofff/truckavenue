package com.example.truckavenue.data.network.entity.truck;

import com.example.truckavenue.data.network.entity.BaseResponse;
import com.squareup.moshi.Json;

import java.util.List;

public class TruckModelResponse extends BaseResponse {

    @Json(name = "result")
    public Result result;

    public static class Result {
        @Json(name = "trucks")
        public List<TruckModel> data = null;
    }

    public static class TruckModel {
        public String id;
        @Json(name = "model")
        public String model;
    }
}
